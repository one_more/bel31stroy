<?php

class PriceUploader {
	use trait_file_uploader;

	private $allowed_extensions = ['xls', 'doc', 'docx', 'xlsx'];
	private $type = 'application';
	private $rename_file = false;
}