<?php

class GalleryController {
	use trait_controller;

	private $path;

	public function __construct($gallery) {
		$this->path = ROOT_PATH.DS.'www'.DS.'starter'.DS.'images'.DS.'galleries'.DS.$gallery;
	}

	public function get_albums() {
		$albums = [];
		$files = glob($this->path.DS.'*');
		foreach($files as $file) {
			if(is_dir($file)) {
				$images = glob($file.DS.'*');
				if(is_array($images) && count($images)) {
					$cover = array_filter($images, function($image) {
						return explode('.', basename($image))[0] == 'cover';
					});
					if(is_array($cover) && count($cover)) {
						$cover = reset($cover);
					} else {
						$cover = $images[0];
					}
					$albums[] = [
						'name' => basename($file),
						'cover' => str_replace(ROOT_PATH.DS.'www', '', $cover)
					];
				}
			}
		}
		return $albums;
	}

	public function get_album($name) {
		$dir = $this->path.DS.$name;
		if(is_dir($dir)) {
			$result = [];
			$files = glob($dir.DS.'*');
			foreach($files as $file) {
				$result[] = str_replace(ROOT_PATH.DS.'www', '', $file);
			}
			return $result;
		} else {
			return [];
		}
	}

	public function load_new_album_images($input_name) {
		$dir_name = md5(json_encode($_FILES));
		$img_uploader = new ImgUploader($input_name);
		$destination_dir = $this->path.DS.$dir_name;
		if(!is_dir($destination_dir)) {
			mkdir($destination_dir, 0777, $recursive = true);
		}
		$img_uploader->upload($destination_dir);
		return [
			'message' => $this->get_lang_vars()['images_loaded']
		];
	}

	public function load_album_images($album, $input_name) {
		$img_uploader = new ImgUploader($input_name);
		$destination_dir = $this->path.DS.$album;
		if(!is_dir($destination_dir)) {
			mkdir($destination_dir);
		}
		$img_uploader->upload($destination_dir);
		return [
			'message' => $this->get_lang_vars()['images_loaded']
		];
	}

	public function change_album_cover($album, $new_cover) {
		$dir = $this->path.DS.$album;
		if(is_dir($dir)) {
			$images = glob($dir.DS.'*');
			$cover = array_filter($images, function($image) {
				return explode('.', basename($image))[0] == 'cover';
			});
			if(is_array($cover) && count($cover)) {
				$cover = reset($cover);
				list($name, $extension) = explode('.', basename($cover));
				rename($cover, $dir.DS.md5_file($cover).".{$extension}");
				$images = glob($dir.DS.'*');
			}
			foreach($images as $img) {
				if(basename($img) == $new_cover) {
					list($name, $extension) = explode('.', basename($img));
					rename($img, $dir.DS."cover.{$extension}");
				}
			}
		}
		return [
			'message' => $this->get_lang_vars()['cover_changed']
		];
	}

	public function remove_album_images($album, $images_names) {
		$dir = $this->path.DS.$album;
		if(is_dir($dir)) {
			if(is_array($images_names)) {
				$images = glob($dir.DS.'*');
				foreach($images_names as $img_name) {
					foreach($images as $img) {
						if(basename($img) == $img_name) {
							unlink($img);
						}
					}
				}
			}
		}
		return [
			'message' => $this->get_lang_vars()['images_removed']
		];
	}

	public function remove_album($name) {
		$dir = $this->path.DS.$name;
		if(is_dir($dir)) {
			Application::remove_dir($dir);
		}
		return [
			'message' => $this->get_lang_vars()['images_removed']
		];
	}
}