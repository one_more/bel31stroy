<?php

class PricesController {
	use trait_controller;

	private $model;
	private $prices_dir;

	public function __construct() {
		$this->model = Application::get_class('PricesModel');
		$this->prices_dir = ROOT_PATH.DS.'www'.DS.'prices';

		if(!is_dir($this->prices_dir)) {
			mkdir($this->prices_dir);
		}
	}

	public function get_prices() {
		return $this->model->get_prices();
	}

	public function add_price($name, $input_name) {
		$lang_vars = $this->get_lang_vars();
		if(empty($name)) {
			return [
				'message' => $lang_vars['enter_name'],
				'status' => 'error'
			];
		}
		if(empty($_FILES[$input_name])) {
			return [
				'message' => $lang_vars['select_file'],
				'status' => 'error'
			];
		}
		$price_uploader = new PriceUploader($input_name);
		$this->model->add_price($name, $this->prices_dir.DS.$_FILES[$input_name]['name']);
		$price_uploader->upload($this->prices_dir);
		return [
			'message' => $lang_vars['success'],
			'status' => 'success'
		];
	}

	public function get_price($id) {
		return $this->model->get_price($id);
	}

	public function update_price($id, $price) {
		$lang_vars = $this->get_lang_vars();
		$old = $this->get_price($id);
		if((!empty($old['file']) && !empty($price['file'])) && $old['file'] != trim($price['file'])) {
			$new_file = trim(basename($price['file']));
			$file_ext = explode('.', basename($old['file']))[1];
			$new_file_name = explode('.', $new_file)[0];
			$new_file_path = $this->prices_dir.DS."{$new_file_name}.{$file_ext}";
			rename($old['file'], $new_file_path);
			$price['file'] = $new_file_path;
		}
		$this->model->update_price($id, $price);
		return [
			'message' => $lang_vars['success'],
			'status' => 'success'
		];
	}

	public function delete_price($id) {
		$lang_vars = $this->get_lang_vars();
		$price = $this->get_price($id);
		$file = $price['file'];
		if(is_file($file)) {
			unlink($file);
		}
		$this->model->delete_price($id);
		return [
			'message' => $lang_vars['success'],
			'status' => 'success'
		];
	}
}