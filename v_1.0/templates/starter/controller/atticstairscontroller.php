<?php

class AtticStairsController {
	use trait_controller;

	private $model;

	public function __construct() {
		$this->model = Application::get_class('AtticStairsModel');
	}

	public function get_stairs($category) {
		return $this->model->get_stairs($category);
	}

	public function add_stairs($fields) {
		return $this->model->add_stairs($fields);
	}

	public function edit_stairs($fields) {
		return $this->model->edit_stairs($fields);
	}

	public function delete_stairs($category, $id) {
		return $this->model->delete_stairs($category, $id);
	}

	public function get_stair($category, $id) {
		return $this->model->get_stair($category, $id);
	}
}