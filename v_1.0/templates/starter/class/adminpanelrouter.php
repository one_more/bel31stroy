<?php

class AdminPanelRouter extends Router {
	use trait_starter_router;

	private $positions = [
		'navbar' => null,
		'left_menu' => null,
		'main_content' => null
	];

	public function __construct() {
		$this->routes = [
			'/admin_panel' => [$this, 'index', 'no check'],
			'/admin_panel/edit_user/:number' => [$this, 'edit_user', 'no check'],
			'/admin_panel/add_user' => [$this, 'add_user', 'no check'],

			'/admin_panel/gallery/:string' => [$this, 'gallery', 'no check'],
			'/admin_panel/gallery/:string/add_album' => [$this, 'add_album', 'no check'],
			'/admin_panel/gallery/:string/load_new_album_images' => [$this, 'load_new_album_images', 'no check'],
			'/admin_panel/remove_gallery_album' => [$this, 'remove_gallery_album', 'no check'],
			'/admin_panel/gallery/:string/:string' => [$this, 'album', 'no check'],
			'/admin_panel/gallery/:string/:string/load_images' => [$this, 'load_album_images', 'no check'],
			'/admin_panel/gallery/:string/:string/remove_images' => [$this, 'remove_album_images', 'no check'],
			'/admin_panel/gallery/:string/:string/change_cover' => [$this, 'change_album_cover'],
			'/admin_panel/prices' => [$this, 'prices', 'no check'],
			'/admin_panel/add_price' => [$this, 'add_price', 'no_check'],
			'/admin_panel/edit_price' => [$this, 'edit_price'],
			'/admin_panel/delete_price' => [$this, 'delete_price'],
			'/admin_panel/attic_stairs/:string' => [$this, 'attic_stairs', 'no check'],
			'/admin_panel/attic_stairs/:string/add_form' => [$this, 'add_attic_stairs_form', 'no check'],
			'/admin_panel/attic_stairs/:string/add' => [$this, 'add_attic_stairs', 'no check'],
			'/admin_panel/attic_stairs/:string/edit_form/:number' => [$this, 'edit_attic_stairs_form', 'no check'],
			'/admin_panel/attic_stairs/:string/edit/:number' => [$this, 'edit_attic_stairs', 'no check'],
			'/admin_panel/attic_stairs/:string/delete/:number' => [$this, 'delete_attic_stairs']
		];
	}

	public function index() {
		$user_controller = Application::get_class('UserController');
		if($user_controller->is_admin()) {
			$users_table_view = Application::get_class('AdminPanelUsersTable');
			$this->positions['main_content'] = $users_table_view->render();
		} else {
			$view = Application::get_class('AdminPanelLogin');
			$this->positions['main_content'] = $view->render();
		}
		$this->show_result();
	}

	public function edit_user($id) {
		$view = Application::get_class('AdminPanelEditUser', [$id]);
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function add_user() {
		$view = Application::get_class('AdminPanelAddUser');
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}


    public function gallery($name) {
		$view = Application::get_class('AdminPanelGalleryView', [$name]);
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function add_album($gallery) {
		$view = Application::get_class('AdminPanelAddAlbumView', [$gallery]);
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function load_new_album_images($gallery) {
		$gallery_controller = Application::get_class('GalleryController', [$gallery]);
		echo json_encode([
			'message' => $gallery_controller->load_new_album_images('images')['message'],
			'status' => 'success'
		]);
	}

	public function load_album_images($gallery, $album) {
		$gallery_controller = Application::get_class('GalleryController', [$gallery]);
		echo json_encode([
			'message' => $gallery_controller->load_album_images($album, 'images')['message'],
			'status' => 'success'
		]);
	}

	public function remove_album_images($gallery, $album) {
		$gallery_controller = Application::get_class('GalleryController', [$gallery]);
		$images = Request::get_var('images', 'raw', []);
		echo json_encode([
			'message' => $gallery_controller->remove_album_images($album, $images)['message'],
			'status' => 'success'
		]);
	}

	public function change_album_cover($gallery, $album) {
		$new_cover = Request::get_var('new_cover', 'string', '');
		$gallery_controller = Application::get_class('GalleryController', [$gallery]);
		echo json_encode([
			'message' => $gallery_controller->change_album_cover($album, $new_cover)['message'],
			'status' => 'success'
		]);
	}

	public function prices() {
		$view = Application::get_class('AdminPanelPricesView');
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function add_price() {
		$name = Request::get_var('name', 'string', '');
		$controller = Application::get_class('PricesController');
		echo json_encode($controller->add_price($name, $file_input = 'price'));
	}

	public function edit_price() {
		$name = Request::get_var('name', 'string', '');
		$id = Request::get_var('pk', 'int', 0);
		$value = Request::get_var('value', 'string', '');
		$controller = Application::get_class('PricesController');
		$price = $controller->get_price($id);
		switch($name) {
			case 'name':
				$price['name'] = $value;
				break;
			case 'file':
				$price['file'] = $value;
				break;
		}
		$controller->update_price($id, $price);
	}

	public function delete_price() {
		$controller = Application::get_class('PricesController');
		$id = Request::get_var('id', 'int', 0);
		echo json_encode($controller->delete_price($id));
	}

	public function remove_gallery_album() {
		$gallery = Request::get_var('gallery', 'string', '');
		$album = Request::get_var('album', 'string', '');
		$gallery_controller = Application::get_class('GalleryController', [$gallery]);
		echo json_encode([
			'message' => $gallery_controller->remove_album($album)['message'],
			'status' => 'success'
		]);
	}

	public function album($gallery, $name) {
		$view = Application::get_class('AdminPanelEditAlbumView', [$gallery, $name]);
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function attic_stairs($category) {
		$view = Application::get_class('AdminPanelAtticStairsView', [$category]);
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function add_attic_stairs_form($category) {
		$view = Application::get_class('AdminPanelAddAtticStairsView', [$category]);
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function add_attic_stairs() {
		$fields = [
			'name' => Request::get_var('name', 'string', ''),
			'category' => Request::get_var('category', 'string', ''),
			'preview' => 'preview',
			'article' => Request::get_var('article', 'raw')
		];
		$controller = Application::get_class('AtticStairsController');
		echo json_encode($controller->add_stairs($fields));
	}

	public function edit_attic_stairs_form($category, $id) {
		$view = Application::get_class('AdminPanelEditAtticStairsView', [$category, $id]);
		$this->positions['main_content'] = $view->render();
		$this->show_result();
	}

	public function edit_attic_stairs() {
		$fields = [
			'name' => Request::get_var('name', 'string', ''),
			'category' => Request::get_var('category', 'string', ''),
			'id' => Request::get_var('id', 'int', ''),
			'preview' => 'preview',
			'article' => Request::get_var('article', 'raw')
		];
		$controller = Application::get_class('AtticStairsController');
		echo json_encode($controller->edit_stairs($fields));
	}

	public function delete_attic_stairs($category, $id) {
		$controller = Application::get_class('AtticStairsController');
		echo json_encode($controller->delete_stairs($category, $id));
	}

	private function show_result() {
		if(!Request::is_ajax()) {
			$template   = Application::get_class('Starter');
			$templator = new Smarty();
			$static_path = DS.'starter';
			$static_paths = [
				'css_path' => $static_path.DS.'css',
				'images_path' => $static_path.DS.'images',
				'js_path' => $static_path.DS.'js'
			];
			$user_controller = Application::get_class('UserController');
			if($user_controller->is_admin()) {
				$left_menu_view = Application::get_class('AdminPanelLeftMenu');
				$this->positions['left_menu'] = $left_menu_view->render();
				$navbar_view = Application::get_class('AdminPanelNavbar');
				$this->positions['navbar'] = $navbar_view->render();
			}
			$templator->assign($static_paths);
			$templator->setTemplateDir($template->path.DS.'templates'.DS.'admin_panel');
			$templator->setCompileDir($template->path.DS.'templates_c');
			$templator->assign($this->positions);
			echo $templator->getTemplate('index.tpl.html');
		} else {
			$this->positions = array_filter($this->positions, function($el) {
				return $el !== null;
			});
			echo json_encode($this->positions);
		}
	}
}