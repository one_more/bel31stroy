<?php

class SiteRouter extends Router {
	use trait_controller, trait_starter_router;

	private $positions = [
		'content' => ''
	];

	public function __construct() {
		$this->routes = [
			'/' => [$this, 'index', 'no check'],
			'/login' => [$this, 'login'],
			'/logout' => [$this, 'logout'],
			'/edit_user' => ['UserController', 'edit_user'],
			'/add_user' => ['UserController', 'add_user'],
			'/language_model' => [$this, 'language_model', 'no check'],
			'/upload_images' => [$this, 'upload_images', 'no check'],
			'/update_article/:name' => [$this, 'update_article'],

			'/our_objects' => [$this, 'our_objects', 'no check'],

			'/constructions/ladders' => [$this, 'ladders', 'no check'],
			'/constructions/roofing' => [$this, 'roofing', 'no check'],
			'/constructions/windows' => [$this, 'windows', 'no check'],

            '/album/:string/:string' => [$this, 'album', 'no check'],

            '/constructions/attic_stairs/:string' => [$this, 'attic_stairs', 'no check'],
			'/constructions/attic_stairs_oman' => [$this, 'attic_stairs_oman', 'no check'],
			'/constructions/attic_stairs/:string/:number' => [$this, 'attic_stair', 'no check'],
			'/contacts' => [$this, 'contacts', 'no check']
		];
	}

	public function index() {
		$view = Application::get_class('MainPageView');
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	public function login() {
		$login = Request::get_var('login', 'string');
		$password = Request::get_var('password', 'string');
		$remember = Request::get_var('remember', 'string');
		$user_controller = Application::get_class('UserController');
		echo json_encode($user_controller->login($login, $password, (bool)$remember));
	}

	public function language_model() {
		$template = Application::get_class('Starter');
		echo file_get_contents($template->path.DS.'lang'.DS.CURRENT_LANG.DS.'client.json');
	}

	public function logout() {
		$user = Application::get_class('User');
		$user->log_out();
	}

	public function upload_images() {
		$user = Application::get_class('User');
		if($user->is_admin()) {
			$img_iploader = new ImgUploader('upload');
			$destination_dir = ROOT_PATH.DS.'www'.DS.'starter'.DS.'images';
			try {
				$url = $img_iploader->upload($destination_dir);
				$url = str_replace(ROOT_PATH.DS.'www', '', $url);
				$CKEditorFuncNum = Request::get_var('CKEditorFuncNum', 'int');
				echo "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url')</script>";
			} catch(Exception $e) {
				echo "<script>{$e->getMessage()}</script>";
			}
		}
	}

	public function update_article($name) {
		$user = Application::get_class('User');
		if($user->is_admin()) {
			$text = trim(Request::get_var('text', 'raw', ''));
			$purifier = HtmlPurifierBuilder::build();
			$text = $purifier->purify($text);
			$articles = Application::get_class('Articles');
			$articles->update_article($name, ['text' => $text]);
		}
	}

	public function our_objects() {
		$this->gallery('houses');
	}

	public function ladders() {
		$this->gallery('stairs');
	}

	public function roofing() {
		$this->gallery('roofing');
	}

	public function windows() {
		$view = Application::get_class('WindowsPageView');
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	private function gallery($name) {
		$view = Application::get_class('GalleryView', [$name]);
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	public function album($gallery, $name) {
		$view = Application::get_class('GalleryAlbumView', [$gallery, $name]);
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	public function attic_stairs($category) {
		$view = Application::get_class('AtticStairsView', [$category]);
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	public function attic_stairs_oman() {
		$view = Application::get_class('AtticStairsOmanView');
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	public function attic_stair($category, $id) {
		$view = Application::get_class('AtticStairView', [$category, $id]);
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	public function contacts() {
		$view = Application::get_class('ContactsPageView');
		$this->positions['content'] = $view->render();
		$this->show_result();
	}

	private function show_result() {
		if(!Request::is_ajax()) {
			$template   = Application::get_class('Starter');
			$templator = new Smarty();
			$static_path = DS.'starter';
			$static_paths = [
				'css_path' => $static_path.DS.'css',
				'images_path' => $static_path.DS.'images',
				'js_path' => $static_path.DS.'js'
			];
			$templator->assign($static_paths);
			$templator->setTemplateDir($template->path.DS.'templates'.DS.'index');
			$templator->setCompileDir($template->path.DS.'templates_c');
			$templator->assign($this->positions);
			$templator->assign('year', date('Y'));
			echo $templator->getTemplate('index.tpl.html');
		} else {
			$this->positions = array_filter($this->positions, function($el) {
				return $el !== null;
			});
			echo json_encode($this->positions);
		}
	}
}