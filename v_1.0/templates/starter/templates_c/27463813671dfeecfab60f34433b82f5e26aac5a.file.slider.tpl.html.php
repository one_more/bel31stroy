<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 14:58:34
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/elastislide/slider.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:205850559155893a311f2ae5-82692322%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '27463813671dfeecfab60f34433b82f5e26aac5a' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/elastislide/slider.tpl.html',
      1 => 1435060710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '205850559155893a311f2ae5-82692322',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55893a312387e0_60306357',
  'variables' => 
  array (
    'data' => 0,
    'el' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55893a312387e0_60306357')) {function content_55893a312387e0_60306357($_smarty_tpl) {?><div id="carousel" class="es-carousel-wrapper">
    <div class="es-carousel">
        <ul>
            <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
?>
            <li>
                <figure class="figure-img">
                    <?php if ($_smarty_tpl->tpl_vars['el']->value['caption']) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['link_more'];?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['el']->value['src'];?>
" alt="">
                    </a>
                    <?php } else { ?>
                    <a><img height="149" src="<?php echo $_smarty_tpl->tpl_vars['el']->value['src'];?>
" alt=""/></a>
                    <?php }?>
                </figure>
                <?php if ($_smarty_tpl->tpl_vars['el']->value['caption']) {?>
                <h3><?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['name'];?>
</h3>
                <p>
                    <?php echo substr($_smarty_tpl->tpl_vars['el']->value['caption']['text'],0,200);?>

                    <a href="<?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['link_more'];?>
" class="link"></a>
                </p>
                <?php }?>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php echo '<script'; ?>
>
    (function() {
        'use strict';

        var init_slider = function() {
            $( '#carousel' ).elastislide();
        };
        if(typeof jQuery == 'undefined') {
            document.addEventListener("DOMContentLoaded", init_slider);
        } else {
            init_slider();
        }
    })();
<?php echo '</script'; ?>
><?php }} ?>
