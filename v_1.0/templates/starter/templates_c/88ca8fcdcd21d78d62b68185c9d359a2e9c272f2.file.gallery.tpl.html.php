<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 17:18:48
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/admin_panel/gallery.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:200473532055896ac822ddd5-44583873%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '88ca8fcdcd21d78d62b68185c9d359a2e9c272f2' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/admin_panel/gallery.tpl.html',
      1 => 1433939402,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '200473532055896ac822ddd5-44583873',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'gallery_name' => 0,
    'add' => 0,
    'gallery' => 0,
    'album' => 0,
    'remove' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55896ac8288199_17424290',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55896ac8288199_17424290')) {function content_55896ac8288199_17424290($_smarty_tpl) {?><div id="gallery">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
            <div id="gallery-preview-img" class="bg-grey">&nbsp;</div>
            <div class="caption">
                <a href="/admin_panel/gallery/<?php echo $_smarty_tpl->tpl_vars['gallery_name']->value;?>
/add_album" id="add-album" class="btn btn-primary"><?php echo $_smarty_tpl->tpl_vars['add']->value;?>
</a>
            </div>
        </div>
    </div>
    <?php  $_smarty_tpl->tpl_vars['album'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['album']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['gallery']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['album']->key => $_smarty_tpl->tpl_vars['album']->value) {
$_smarty_tpl->tpl_vars['album']->_loop = true;
?>
    <div class="col-sm-6 col-md-4">
        <a class="thumbnail" href="/admin_panel/gallery/<?php echo $_smarty_tpl->tpl_vars['gallery_name']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['album']->value['name'];?>
">
            <img id="gallery-preview-img" src="<?php echo $_smarty_tpl->tpl_vars['album']->value['cover'];?>
?q=<?php echo rand(1,10000);?>
" alt=""/>
            <div class="caption">
                <button id="remove-album" data-gallery="<?php echo $_smarty_tpl->tpl_vars['gallery_name']->value;?>
" data-album="<?php echo $_smarty_tpl->tpl_vars['album']->value['name'];?>
" class="btn btn-danger"><?php echo $_smarty_tpl->tpl_vars['remove']->value;?>
</button>
            </div>
        </a>
    </div>
    <?php } ?>
</div><?php }} ?>
