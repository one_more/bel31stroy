<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-24 16:07:31
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/admin_panel/attic_stairs_edit.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:662162515558aab93cb6168-20880302%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6fe5eaf0a9d8db6ff16535189c15651f3437a6b' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/admin_panel/attic_stairs_edit.tpl.html',
      1 => 1435151143,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '662162515558aab93cb6168-20880302',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'id' => 0,
    'name' => 0,
    'stair' => 0,
    'preview' => 0,
    'article' => 0,
    'save' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_558aab93ce4005_41300461',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_558aab93ce4005_41300461')) {function content_558aab93ce4005_41300461($_smarty_tpl) {?><form id="attic-stairs-edit-form" action="/admin_panel/attic_stairs/<?php echo $_smarty_tpl->tpl_vars['category']->value;?>
/edit/<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
">
    <input type="hidden" name="category" value="<?php echo $_smarty_tpl->tpl_vars['category']->value;?>
"/>
    <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
"/>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label for="name"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</label>
                <input value="<?php echo $_smarty_tpl->tpl_vars['stair']->value['name'];?>
" id="name" name="name" class="form-control" type="text"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <label for="preview"><?php echo $_smarty_tpl->tpl_vars['preview']->value;?>
</label>
                <input id="preview" name="preview" type="file"/>
                <img style="max-width: 160px; max-height: 160px" src="<?php echo $_smarty_tpl->tpl_vars['stair']->value['preview'];?>
" alt=""/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label for="article"><?php echo $_smarty_tpl->tpl_vars['article']->value;?>
</label>
                <textarea id="article" name="article" class="form-control">
                    <?php echo $_smarty_tpl->tpl_vars['stair']->value['article'];?>

                </textarea>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <input class="btn btn-success" value="<?php echo $_smarty_tpl->tpl_vars['save']->value;?>
" type="submit"/>
            </div>
        </div>
    </div>
</form><?php }} ?>
