<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 16:22:45
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/admin_panel/users_table.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:94428849255895da5810796-94791894%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '55b1d74dec3ef0b0e68dbc053654c72ab826a9b7' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/admin_panel/users_table.tpl.html',
      1 => 1433520560,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '94428849255895da5810796-94791894',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'login' => 0,
    'password' => 0,
    'credentials' => 0,
    'users' => 0,
    'user' => 0,
    'credentials_labels' => 0,
    'is_super_admin' => 0,
    'edit' => 0,
    'add_user' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55895da5968498_60090126',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55895da5968498_60090126')) {function content_55895da5968498_60090126($_smarty_tpl) {?><table class="table table-striped">
    <thead>
        <tr>
            <th><?php echo $_smarty_tpl->tpl_vars['login']->value;?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['password']->value;?>
</th>
            <th><?php echo $_smarty_tpl->tpl_vars['credentials']->value;?>
</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value) {
$_smarty_tpl->tpl_vars['user']->_loop = true;
?>
            <tr>
                <td><?php echo $_smarty_tpl->tpl_vars['user']->value['login'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['user']->value['password'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['credentials_labels']->value[$_smarty_tpl->tpl_vars['user']->value['credentials']];?>
</td>
                <td width="30">
                    <?php if ($_smarty_tpl->tpl_vars['is_super_admin']->value) {?>
                        <a href="/admin_panel/edit_user/<?php echo $_smarty_tpl->tpl_vars['user']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['edit']->value;?>
</a>
                    <?php }?>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php if ($_smarty_tpl->tpl_vars['is_super_admin']->value) {?>
    <a class="btn btn-primary" href="/admin_panel/add_user"><?php echo $_smarty_tpl->tpl_vars['add_user']->value;?>
</a>
<?php }?><?php }} ?>
