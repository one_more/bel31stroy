<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 17:25:37
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/admin_panel/prices.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:175671770355896c61c2a3c6-08215484%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9cac7d847aabce73ee7262f6c7f973fa32063368' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/admin_panel/prices.tpl.html',
      1 => 1434289177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '175671770355896c61c2a3c6-08215484',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'name' => 0,
    'file' => 0,
    'prices' => 0,
    'el' => 0,
    'delete' => 0,
    'new_price' => 0,
    'add' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55896c61c7d708_51431558',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55896c61c7d708_51431558')) {function content_55896c61c7d708_51431558($_smarty_tpl) {?><div id="prices" class="margin-bottom-30px">
    <table class="table table-striped">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</th>
                <th><?php echo $_smarty_tpl->tpl_vars['file']->value;?>
</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['prices']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
?>
        <tr>
            <td>
                <a href="#" id="name" data-pk="<?php echo $_smarty_tpl->tpl_vars['el']->value['id'];?>
" data-type="text" data-url="/admin_panel/edit_price"><?php echo $_smarty_tpl->tpl_vars['el']->value['name'];?>
</a>
            </td>
            <td>
                <a href="#" id="file" data-pk="<?php echo $_smarty_tpl->tpl_vars['el']->value['id'];?>
" data-type="text" data-url="/admin_panel/edit_price"><?php echo basename($_smarty_tpl->tpl_vars['el']->value['file']);?>
</a>
            </td>
            <td width="40">
                <button class="btn btn-danger btn-xs" id="delete-price" data-id="<?php echo $_smarty_tpl->tpl_vars['el']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['delete']->value;?>
</button>
            </td>
        </tr>
        <?php } ?>
        </tbody>
    </table>
    <button class="btn btn-primary" id="toggle-add-form"><?php echo $_smarty_tpl->tpl_vars['new_price']->value;?>
</button>
    <form class="form-inline margin-top-30px hide" id="add-price-form" action="/admin_panel/add_price">
        <div class="form-group">
            <label for="price-name"><?php echo $_smarty_tpl->tpl_vars['name']->value;?>
</label>
            <input name="name" type="text" class="form-control" id="price-name" />
        </div>
        <div class="form-group">
            <input name="price" type="file"/>
        </div>
        <input class="btn btn-info" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['add']->value;?>
"/>
    </form>
</div><?php }} ?>
