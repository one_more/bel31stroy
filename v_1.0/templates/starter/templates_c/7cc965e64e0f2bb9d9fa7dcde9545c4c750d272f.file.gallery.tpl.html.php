<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 14:58:44
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/gallery/gallery.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:2123863845558933d6a6eac9-12529691%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7cc965e64e0f2bb9d9fa7dcde9545c4c750d272f' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/gallery/gallery.tpl.html',
      1 => 1435059526,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2123863845558933d6a6eac9-12529691',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_558933d6aba031_77837095',
  'variables' => 
  array (
    'gallery' => 0,
    'galleries' => 0,
    'albums' => 0,
    'album' => 0,
    'album_link' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_558933d6aba031_77837095')) {function content_558933d6aba031_77837095($_smarty_tpl) {?><div class="container_24">
    <div class="wrapper">
        <article class="grid_24">
            <h2 class="bot"><?php echo $_smarty_tpl->tpl_vars['galleries']->value[$_smarty_tpl->tpl_vars['gallery']->value];?>
</h2>
        </article>
        <?php  $_smarty_tpl->tpl_vars['album'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['album']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['albums']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['album']->key => $_smarty_tpl->tpl_vars['album']->value) {
$_smarty_tpl->tpl_vars['album']->_loop = true;
?>
        <?php $_smarty_tpl->tpl_vars['album_link'] = new Smarty_variable(((("/album/").($_smarty_tpl->tpl_vars['gallery']->value)).("/")).($_smarty_tpl->tpl_vars['album']->value['name']), null, 0);?>
        <article class="grid_8">
            <div class="block maxheight">
                <figure class="figure-img">
                    <a href="<?php echo $_smarty_tpl->tpl_vars['album_link']->value;?>
">
                        <img height="179" src="<?php echo $_smarty_tpl->tpl_vars['album']->value['cover'];?>
" alt="">
                    </a>
                </figure>
                <?php if (isset($_smarty_tpl->tpl_vars['album']->value['caption'])) {?>
                <?php if (isset($_smarty_tpl->tpl_vars['album']->value['caption']['name'])) {?>
                <h3><?php echo $_smarty_tpl->tpl_vars['album']->value['caption']['name'];?>
</h3>
                <?php }?>
                <?php if (isset($_smarty_tpl->tpl_vars['album']->value['caption']['text'])) {?>
                <p><?php echo $_smarty_tpl->tpl_vars['album']->value['caption']['text'];?>
<a href="<?php echo $_smarty_tpl->tpl_vars['album_link']->value;?>
" class="link"></a></p>
                <?php }?>
                <?php }?>
            </div>
        </article>
        <?php } ?>
    </div>
</div><?php }} ?>
