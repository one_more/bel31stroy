<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 14:58:34
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/flex_slider/slider.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:187095978255893a31177e04-76556168%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1073787c962d9e3180c0f4bc9063ee97617242a6' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/flex_slider/slider.tpl.html',
      1 => 1435060710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '187095978255893a31177e04-76556168',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55893a311ed495_51662188',
  'variables' => 
  array (
    'data' => 0,
    'el' => 0,
    'more' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55893a311ed495_51662188')) {function content_55893a311ed495_51662188($_smarty_tpl) {?><div class="main-slider">
    <div class="container_24">
        <div class="flexslider clearfix">
            <ul class="slides">
                <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
?>
                <li>
                    <img alt="" height="470" src="<?php echo $_smarty_tpl->tpl_vars['el']->value['src'];?>
">
                    <?php if ($_smarty_tpl->tpl_vars['el']->value['caption']) {?>
                    <div class="flex-caption">
                        <div class="banner">
                            <div class="inner">
                                <span><?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['name'];?>
</span>
                                <div class="wrapper">
                                    <div class="fleft">
                                        <?php echo substr($_smarty_tpl->tpl_vars['el']->value['caption']['text'],0,200);?>

                                    </div>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['link_more'];?>
" class="banner-button">
                                        <?php echo $_smarty_tpl->tpl_vars['more']->value;?>

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<?php echo '<script'; ?>
>
    (function() {
        'use strict';

        var init_slider = function() {
            jQuery('.flexslider').flexslider({
                animation: "slide",
                slideshow: true,
                slideshowSpeed: 7000,
                animationDuration: 600,
                prevText: "",
                nextText: "",
                controlNav: true
            });
        };
        if(typeof jQuery == 'undefined') {
            document.addEventListener("DOMContentLoaded", init_slider);
        } else {
            init_slider();
        }
    })();
<?php echo '</script'; ?>
><?php }} ?>
