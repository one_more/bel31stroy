<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-10-19 16:53:43
         compiled from "/var/www/bel31stroy.my/v_1.0/templates/starter/templates/elastislide/slider.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:12303571485624f5e7643194-59935187%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'dbce63e48fbd797a73ea99cbbf914d0e77d9a67d' => 
    array (
      0 => '/var/www/bel31stroy.my/v_1.0/templates/starter/templates/elastislide/slider.tpl.html',
      1 => 1435060710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12303571485624f5e7643194-59935187',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'data' => 0,
    'el' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5624f5e766b864_03691998',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5624f5e766b864_03691998')) {function content_5624f5e766b864_03691998($_smarty_tpl) {?><div id="carousel" class="es-carousel-wrapper">
    <div class="es-carousel">
        <ul>
            <?php  $_smarty_tpl->tpl_vars['el'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['el']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['el']->key => $_smarty_tpl->tpl_vars['el']->value) {
$_smarty_tpl->tpl_vars['el']->_loop = true;
?>
            <li>
                <figure class="figure-img">
                    <?php if ($_smarty_tpl->tpl_vars['el']->value['caption']) {?>
                    <a href="<?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['link_more'];?>
">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['el']->value['src'];?>
" alt="">
                    </a>
                    <?php } else { ?>
                    <a><img height="149" src="<?php echo $_smarty_tpl->tpl_vars['el']->value['src'];?>
" alt=""/></a>
                    <?php }?>
                </figure>
                <?php if ($_smarty_tpl->tpl_vars['el']->value['caption']) {?>
                <h3><?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['name'];?>
</h3>
                <p>
                    <?php echo substr($_smarty_tpl->tpl_vars['el']->value['caption']['text'],0,200);?>

                    <a href="<?php echo $_smarty_tpl->tpl_vars['el']->value['caption']['link_more'];?>
" class="link"></a>
                </p>
                <?php }?>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php echo '<script'; ?>
>
    (function() {
        'use strict';

        var init_slider = function() {
            $( '#carousel' ).elastislide();
        };
        if(typeof jQuery == 'undefined') {
            document.addEventListener("DOMContentLoaded", init_slider);
        } else {
            init_slider();
        }
    })();
<?php echo '</script'; ?>
><?php }} ?>
