<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 17:23:29
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/admin_panel/edit_album.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:102008376355896be1a246c4-14453686%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '48d267edea5ba9e7594eba644617fbfe37857612' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/admin_panel/edit_album.tpl.html',
      1 => 1433939372,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '102008376355896be1a246c4-14453686',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'gallery' => 0,
    'album' => 0,
    'select_photo' => 0,
    'images' => 0,
    'image' => 0,
    'remove' => 0,
    'make_cover' => 0,
    'save' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_55896be1a7e683_20120045',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55896be1a7e683_20120045')) {function content_55896be1a7e683_20120045($_smarty_tpl) {?><form id="edit-album" action="/admin_panel/gallery/<?php echo $_smarty_tpl->tpl_vars['gallery']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['album']->value;?>
/load_images">
    <input name="gallery" value="<?php echo $_smarty_tpl->tpl_vars['gallery']->value;?>
" type="hidden"/>
    <input name="album" value="<?php echo $_smarty_tpl->tpl_vars['album']->value;?>
" type="hidden"/>
    <div class="row">
        <div class="col-lg-4 padding-right-5px">
            <input name="photos" readonly class="form-control" id="photos-names" type="text"/>
        </div>
        <div class="col-lg-4 padding-0">
            <button class="btn btn-primary" id="select-photo"><?php echo $_smarty_tpl->tpl_vars['select_photo']->value;?>
</button>
        </div>
        <input name="images" type="file" multiple class="opacity-0"/>
    </div>
    <div class="row">
        <div id="thumbnails">
            <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
            <div class="thumbnail display-inline-block">
                <img id="album-preview-img" src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
?q=<?php echo rand(1,10000);?>
" alt=""/>
                <div class="caption">
                    <button id="remove-img" data-gallery="<?php echo $_smarty_tpl->tpl_vars['gallery']->value;?>
" data-album="<?php echo $_smarty_tpl->tpl_vars['album']->value;?>
" class="btn btn-danger btn-xs"><?php echo $_smarty_tpl->tpl_vars['remove']->value;?>
</button>
                    <button id="make-cover" data-gallery="<?php echo $_smarty_tpl->tpl_vars['gallery']->value;?>
" data-album="<?php echo $_smarty_tpl->tpl_vars['album']->value;?>
" class="btn btn-info btn-xs"><?php echo $_smarty_tpl->tpl_vars['make_cover']->value;?>
</button>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <input class="btn btn-success" type="submit" value="<?php echo $_smarty_tpl->tpl_vars['save']->value;?>
"/>
            </div>
        </div>
    </div>
</form><?php }} ?>
