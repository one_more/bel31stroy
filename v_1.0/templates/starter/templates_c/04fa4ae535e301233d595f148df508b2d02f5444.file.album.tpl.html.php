<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-06-23 15:43:32
         compiled from "/var/www/bel31stroy.my/templates/starter/templates/album/album.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:2025560825558949f7284c92-19647731%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '04fa4ae535e301233d595f148df508b2d02f5444' => 
    array (
      0 => '/var/www/bel31stroy.my/templates/starter/templates/album/album.tpl.html',
      1 => 1435063412,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2025560825558949f7284c92-19647731',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_558949f72977d3_67210466',
  'variables' => 
  array (
    'images' => 0,
    'image' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_558949f72977d3_67210466')) {function content_558949f72977d3_67210466($_smarty_tpl) {?><div id="grid-gallery" class="grid-gallery">
    <section class="grid-wrap">
        <ul class="grid">
            <li class="grid-sizer"></li><!-- for Masonry column width -->
            <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
            <li>
                <figure>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" alt=""/>
                </figure>
            </li>
            <?php } ?>
        </ul>
    </section>
    <section class="slideshow">
        <ul>
            <?php  $_smarty_tpl->tpl_vars['image'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['image']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['images']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['image']->key => $_smarty_tpl->tpl_vars['image']->value) {
$_smarty_tpl->tpl_vars['image']->_loop = true;
?>
            <li>
                <figure>
                    <img src="<?php echo $_smarty_tpl->tpl_vars['image']->value;?>
" alt=""/>
                </figure>
            </li>
            <?php } ?>
        </ul>
        <nav>
            <span class="icon icon-arrow-left nav-prev"></span>
            <span class="icon icon-arrow-right nav-next"></span>
            <span class="icon icon-cross nav-close"></span>
        </nav>
    </section>
</div>
<?php echo '<script'; ?>
>
    (function() {
        'use strict';

        var init_gallery = function() {
            new CBPGridGallery( document.getElementById( 'grid-gallery' ) );
        };
        if(typeof CBPGridGallery == 'undefined') {
            document.addEventListener("DOMContentLoaded", init_gallery);
        } else {
            init_gallery();
        }
    })();
<?php echo '</script'; ?>
>
<?php }} ?>
