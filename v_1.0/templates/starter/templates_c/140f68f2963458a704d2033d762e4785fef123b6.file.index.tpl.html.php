<?php /* Smarty version Smarty-3.1.21-dev, created on 2015-10-19 16:53:43
         compiled from "/var/www/bel31stroy.my/v_1.0/templates/starter/templates/index/index.tpl.html" */ ?>
<?php /*%%SmartyHeaderCode:2072210695624f5e76b0d70-88646092%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '140f68f2963458a704d2033d762e4785fef123b6' => 
    array (
      0 => '/var/www/bel31stroy.my/v_1.0/templates/starter/templates/index/index.tpl.html',
      1 => 1435222967,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2072210695624f5e76b0d70-88646092',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'images_path' => 0,
    'css_path' => 0,
    'content' => 0,
    'year' => 0,
    'js_path' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5624f5e76c9849_52065373',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5624f5e76c9849_52065373')) {function content_5624f5e76c9849_52065373($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <link rel="icon" href="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_smarty_tpl->tpl_vars['css_path']->value;?>
/template.css">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 8]>
    <div style=' clear: both; text-align:center; position: relative;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
            <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
        </a>
    </div>
    <![endif]-->
    <!--[if lt IE 9]>
    <?php echo '<script'; ?>
 type="text/javascript" src="js/html5.js"><?php echo '</script'; ?>
>
    <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
</head>
<body id="pages">
    <div class="main">
        <!--==============================header=================================-->
        <header>
            <div class="container_24">
                <div class="wrapper">
                    <h1>
                        <a href="/">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/logo_new.png" alt="">
                        </a><span>InterDom architecture company</span></h1>
                    <!--
                    <div class="social">
                        <a href="#"><img src="images/icon.png" alt=""></a>
                        <a href="#"><img src="images/icon1.png" alt=""></a>
                        <a href="#"><img src="images/icon2.png" alt=""></a>
                        <a href="#"><img src="images/icon3.png" alt=""></a>
                    </div>
                    -->
                </div>
            </div>
            <div class="nav-div">
                <div class="container_24">
                    <nav>
                        <ul class="sf-menu">
                            <li><a href="/">Главная</a></li>
                            <li><a href="/our_objects">Наши объекты</a></li>
                            <li class="sub-menu"><a>Стройконструкции</a>
                                <ul>
                                    <li><a href="/constructions/ladders">Лестницы</a></li>
                                    <li><a href="/constructions/roofing">Кровля</a></li>
                                    <li><a href="/constructions/windows">Окна</a></li>
                                    <li><a>Чердачные лестницы</a>
                                        <ul>
                                            <li>
                                                <a href="/constructions/attic_stairs/facro">fakro</a>
                                            </li>
                                            <li>
                                                <a href="/constructions/attic_stairs_oman">oman</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="/contacts">Контакты</a></li>
                        </ul>
                        <div class="clear"></div>
                    </nav>
                    <!--
                    <div class="div-search">
                        <form id="search" action="search.php" method="GET" accept-charset="utf-8">
                            <input type="text" name="s" value=""/>
                            <a onClick="document.getElementById('search').submit()"  class="search_button">search</a>
                        </form>
                    </div>
                    -->
                    <div class="clear"></div>
                </div>
            </div>
        </header>

        <!--==============================content================================-->
        <section id="content">
            <?php echo $_smarty_tpl->tpl_vars['content']->value;?>

        </section>
        <!--==============================footer=================================-->
        <footer>
            <article class="container_24">
                <article class="grid_12">
                    <a href="/">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/logo-1_new.png" alt=""></a>&nbsp;&nbsp;&copy; <?php echo $_smarty_tpl->tpl_vars['year']->value;?>
<br>
                </article>
                <article class="grid_12">
                    <div class="txt-alr">
                        +7 (4722) 50 63 13 &nbsp; | &nbsp; +7(903) 887 12 22 &nbsp;
                        <img src="<?php echo $_smarty_tpl->tpl_vars['images_path']->value;?>
/img-phone.png" alt="">
                    </div>
                </article>
            </article>
        </footer>
        <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['js_path']->value;?>
/template.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 type="text/javascript">
            $(window).load(function() {
                $().UItoTop({ easingType: 'easeOutQuart' });
            });
        <?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="/ck_editor/ckeditor.js"><?php echo '</script'; ?>
>
    </div>
</body>
</html><?php }} ?>
