<?php

class PricesModel {
	use trait_json;

	private $file;
	private $data;

	public function __construct() {
		$template = Application::get_class('Starter');
		$this->file = $template->path.DS.'lang'.DS.CURRENT_LANG.DS.'prices.json';
		$this->load_data();
	}

	private function load_data() {
		$this->data = json_decode(file_get_contents($this->file), $assoc = true);
	}

	public function get_prices() {
		return $this->data;
	}

	public function add_price($name, $file_path) {
		if(empty($name)) {
			throw new InvalidArgumentException('specify price name');
		}
		if(empty($file_path)) {
			throw new InvalidArgumentException('specify price file');
		}
		$this->data[] = [
			'id' => count($this->data)+1,
			'name' => $name,
			'file' => $file_path
		];
		$this->save_data();
	}

	public function get_price($id) {
		$id = (int)$id;
		$price = array_filter($this->data, function($el) use($id) {
			return $el['id'] == $id;
		});
		return reset($price);
	}

	public function update_price($id, $price) {
		foreach($this->data as &$el) {
			if($el['id'] == $id) {
				$el = array_merge($el, $price);
			}
		}
		$this->save_data();
	}

	public function delete_price($id) {
		$id = (int)$id;
		foreach($this->data as $k=>$el) {
			if($el['id'] == $id) {
				unset($this->data[$k]);
			}
		}
		$this->save_data();
	}

	private function save_data() {
		file_put_contents($this->file, $this->array_to_json_string($this->data));
	}
}