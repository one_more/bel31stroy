<?php

class AtticStairsModel {
	use trait_json, trait_controller;

	private $file;
	private $data;

	public function __construct() {
		$template = Application::get_class('Starter');
		$this->file = $template->path.DS.'lang'.DS.CURRENT_LANG.DS.'attic_stairs.json';
		$this->load_data();
	}

	private function load_data() {
		$this->data = json_decode(file_get_contents($this->file), $assoc = true);
		$articles = Application::get_class('Articles');
		foreach($this->data as $category=>&$records) {
			$cat_stairs = $articles->get_category($category);
			foreach($records as &$el) {
				foreach($cat_stairs as $cat_stair) {
					if($el['article_id'] == $cat_stair['id']) {
						$el['article'] = $cat_stair['text'];
					}
				}
			}
		}
	}

	public function get_stairs($category) {
		if(empty($this->data[$category])) {
			return [];
		} else {
			return $this->data[$category];
		}
	}

	public function add_stairs($fields) {
		$lang_vars = $this->get_lang_vars();
		$purifier = HtmlPurifierBuilder::build();
		$fields['article'] = $purifier->purify(trim($fields['article']));
		if(empty($_FILES[$fields['preview']]['name'])) {
			return [
				'status' => 'error',
				'message' => $lang_vars['entry_preview']
			];
		}
		if($error = $this->check_fields($fields)) {
			return [
				'status' => 'error',
				'message' => $error
			];
		}
		$fields['preview'] = $this->upload_preview($fields['preview']);
		$articles = Application::get_class('Articles');
		$article_fields = [
			'name' => $fields['name'],
			'category' => $fields['category'],
			'text' => $fields['article']
		];
		$fields['article_id'] = $articles->add_article($article_fields);
		$fields['id'] = !empty($this->data[$fields['category']]) ?
			count($this->data[$fields['category']])+1 :
			1;
		$this->data[$fields['category']][] = $fields;
		$this->save_data();
		return [
			'status' => 'success',
			'message' => $lang_vars['added']
		];
	}

	private function check_fields($fields) {
		$lang_vars = $this->get_lang_vars();
		if(empty($fields['category'])) {
			return $lang_vars['entry_category'];
		}
		if(empty($fields['name'])) {
			return $lang_vars['entry_name'];
		}
		if(empty($fields['article'])) {
			return $lang_vars['entry_article'];
		}
		return '';
	}

	private function upload_preview($input_name) {
		$img_uploader = new ImgUploader($input_name);
		$destination_dir = ROOT_PATH.DS.'www'.DS.'starter'.DS.'images';
		return str_replace(ROOT_PATH.DS.'www', '', $img_uploader->upload($destination_dir));
	}

	public function edit_stairs($fields) {
		if($error = $this->check_fields($fields)) {
			return [
				'status' => 'error',
				'message' => $error
			];
		}
		if(empty($this->data[$fields['category']]) || empty($fields['id'])) {
			throw new InvalidArgumentException('invalid id or category');
		}
		$lang_vars = $this->get_lang_vars();
		$purifier = HtmlPurifierBuilder::build();
		$fields['article'] = $purifier->purify(trim($fields['article']));
		$controller = Application::get_class('AtticStairsController');
		$old_record = $controller->get_stair($fields['category'], $fields['id']);
		if(!empty($_FILES[$fields['preview']]['name'])) {
			$fields['preview'] = $this->upload_preview($fields['preview']);
			$file = ROOT_PATH.DS.'www'.DS.$old_record['preview'];
			unlink($file);
		} else {
			unset($fields['preview']);
		}
		$article_fields = [
			'name' => $fields['name'],
			'text' => $fields['article']
		];
		$articles = Application::get_class('Articles');
		$articles->update_article($old_record['article_id'], $article_fields);
		foreach($this->data[$fields['category']] as &$el) {
			if($el['id'] == $fields['id']) {
				$el = array_merge($el, $fields);
			}
		}
		$this->save_data();
		return [
			'status' => 'success',
			'message' => $lang_vars['edited']
		];
	}

	public function get_stair($category, $id) {
		$records = array_filter($this->get_stairs($category), function($el) use($id) {
			return $el['id'] == $id;
		});
		return reset($records);
	}

	public function delete_stairs($category, $id) {
		$lang_vars = $this->get_lang_vars();
		foreach($this->data[$category] as $k=>$el) {
			if($el['id'] == $id) {
				$preview_file = ROOT_PATH.DS.'www'.DS.$el['preview'];
				unlink($preview_file);
				$articles = Application::get_class('Articles');
				$articles->delete_article($el['article_id']);
				unset($this->data[$category][$k]);
				$this->save_data();
				break;
			}
		}
		return [
			'status' => 'success',
			'message' => $lang_vars['deleted']
		];
	}

	private function save_data() {
		$data = $this->data;
		foreach($data as &$cat) {
			foreach($cat as &$el) {
				unset($el['article']);
			}
		}
		file_put_contents($this->file, $this->array_to_json_string($data));
	}
}