<?php

class FlexSliderView extends TemplateView {

	public function __construct($page = 'main') {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'flex_slider');
		$this->load_data($page);
	}

	private function load_data($page) {
		switch($page) {
			case 'main':
				default:
				$controller = new GalleryController('houses');
				$albums = $controller->get_albums();
				$albums = array_slice($albums, 0, 6);
				$data = [];
				foreach($albums as $album) {
					$data[] = [
						'caption' => '',
						'src' => $album['cover']
					];
				}
				$this->assign('data', $data);
				break;
		}
	}

	public function render() {
		return $this->getTemplate('slider.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'flex_slider_view.json';
	}
}