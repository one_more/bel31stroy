<?php

class WindowsPageView extends TemplateView {

	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'windows');
	}

	public function render() {
		$view = new GalleryView('windows');
		$this->assign('gallery', $view->render());
		$images = glob(ROOT_PATH.DS.'www'.DS.'starter'.DS.'images'.DS.'windows_facro'.DS.'*');
		$this->assign('images', array_map(function($el){
			return str_replace(ROOT_PATH.DS.'www', '', $el);
		}, $images));
		return $this->getTemplate('windows.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'windows_page_view.json';
	}
}