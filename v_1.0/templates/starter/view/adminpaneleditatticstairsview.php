<?php

class AdminPanelEditAtticStairsView extends TemplateView {

	public function __construct($category, $id) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
		$this->assign('category', $category);
		$this->assign('id', $id);
		$controller = Application::get_class('AtticStairsController');
		$this->assign('stair', $controller->get_stair($category, $id));
	}

	public function render() {
		return $this->getTemplate('attic_stairs_edit.tpl.html');
	}

	public function get_lang_file() {
		$lang_file = 'admin_panel_add_attic_stairs_view.json';
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.$lang_file;
	}
}