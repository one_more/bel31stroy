<?php

class AdminPanelPricesView extends TemplateView {

	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
	}

	public function render() {
		$controller = Application::get_class('PricesController');
		$this->assign('prices', $controller->get_prices());
		return $this->getTemplate('prices.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_prices_view.json';
	}
}