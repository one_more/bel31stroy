<?php

class AtticStairView extends TemplateView {
	public function __construct($category, $id) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'attic_stairs');
		$controller = Application::get_class('AtticStairsController');
		$this->assign('stair', $controller->get_stair($category, $id));
	}

	public function render() {
		return $this->getTemplate('stair.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'attic_stair_view.json';
	}
}