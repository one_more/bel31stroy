<?php

class AdminPanelAddAlbumView extends TemplateView {

	public function __construct($gallery) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
		$this->assign('gallery', $gallery);
	}

	public function render() {
		return $this->getTemplate('add_album.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_add_album_view.json';
	}
}