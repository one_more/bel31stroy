<?php

class ContactsPageView extends TemplateView {

	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'contacts');
	}

	public function render() {
		return $this->getTemplate('contacts.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'contacts_page_view.json';
	}
}