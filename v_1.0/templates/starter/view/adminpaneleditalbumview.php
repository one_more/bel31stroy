<?php

class AdminPanelEditAlbumView extends TemplateView {

	public function __construct($gallery, $album) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
		$this->assign('gallery', $gallery);
		$this->assign('album', $album);
		$controller = Application::get_class('GalleryController', [$gallery]);
		$this->assign('images', $controller->get_album($album));
	}

	public function render() {
		return $this->getTemplate('edit_album.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_edit_album_view.json';
	}
}