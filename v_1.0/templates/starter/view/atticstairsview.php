<?php

class AtticStairsView extends TemplateView {
	public function __construct($category) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'attic_stairs');
		$controller = Application::get_class('AtticStairsController');
		$this->assign('stairs', $controller->get_stairs($category));
		$this->assign('category', $category);
	}

	public function render() {
		return $this->getTemplate('stairs.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'attic_stairs_view.json';
	}
}