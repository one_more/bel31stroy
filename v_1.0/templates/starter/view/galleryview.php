<?php

class GalleryView extends TemplateView {
	public function __construct($name) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'gallery');
		$controller = new GalleryController($name);
		$this->assign('albums', $controller->get_albums());
		$this->assign('gallery', $name);
	}

	public function render() {
		return $this->getTemplate('gallery.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'gallery_view.json';
	}
}