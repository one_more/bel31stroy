<?php

class PricesView extends TemplateView {
	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'prices');
	}

	public function render() {
		$controller = Application::get_class('PricesController');
		$prices = array_map(function($el) {
			$el['file'] = str_replace(ROOT_PATH.DS.'www', '', $el['file']);
			return $el;
		}, $controller->get_prices());
		$this->assign('prices', $prices);
		return $this->getTemplate('prices.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'prices_view.json';
	}
}