<?php

class GalleryAlbumView extends TemplateView {
	public function __construct($gallery, $name) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'album');
		$controller = new GalleryController($gallery);
		$this->assign('images', $controller->get_album($name));
	}

	public function render() {
		return $this->getTemplate('album.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'gallery_album_view.json';
	}
}