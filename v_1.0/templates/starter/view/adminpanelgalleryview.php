<?php

class AdminPanelGalleryView extends TemplateView {

	public function __construct($gallery) {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'admin_panel');
		$this->assign('gallery_name', $gallery);
		$gallery_controller = Application::get_class('GalleryController', [$gallery]);
		$this->assign('gallery', $gallery_controller->get_albums());
	}

	public function render() {
		return $this->getTemplate('gallery.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'admin_panel_gallery_view.json';
	}
}