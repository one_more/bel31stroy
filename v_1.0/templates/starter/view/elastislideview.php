<?php

class ElastiSlideView extends TemplateView {

	public function __construct($page = 'main') {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'elastislide');
		$this->load_data($page);
	}

	private function load_data($page) {
		switch($page) {
			case 'main':
				default:
				$controller = new GalleryController('recent_works');
				$images = $controller->get_album($controller->get_albums()[0]['name']);
				$data = [];
				foreach($images as $img) {
					$data[] = [
						'src' => $img,
						'caption' => []
					];
				}
				$this->assign('data', $data);
				break;
		}
	}

	public function render() {
		return $this->getTemplate('slider.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'main_page_view.json';
	}
}