<?php

class MainPageView extends TemplateView {

	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'main');
	}

	public function render() {
		$view = Application::get_class('FlexSliderView');
		$this->assign('slider', $view->render());
		$view = Application::get_class('ElastiSlideView');
		$this->assign('carousel', $view->render());
		$view = Application::get_class('PricesView');
		$this->assign('prices_list', $view->render());
		$articles = Application::get_class('Articles');
		$this->assign('welcome_article', $articles->get_article('welcome_article'));
		$user = Application::get_class('User');
		$this->assign('is_admin', $user->is_admin());
		return $this->getTemplate('main.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'main_page_view.json';
	}
}