<?php

class AtticStairsOmanView extends TemplateView {
	public function __construct() {
		parent::__construct();
		$this->setTemplateDir($this->template->path.DS.'templates'.DS.'attic_stairs_oman');
	}

	public function render() {
		$images = glob(ROOT_PATH.DS.'www'.DS.'starter'.DS.'images'.DS.'attic_stairs_oman'.DS.'*');
		$this->assign('images', array_map(function($el){
			return str_replace(ROOT_PATH.DS.'www', '', $el);
		}, $images));
		return $this->getTemplate('oman.tpl.html');
	}

	public function get_lang_file() {
		return $this->template->path.DS.'lang'.DS.CURRENT_LANG.DS.'attic_stairs_oman_view.json';
	}
}