<?php

class Articles {
	use trait_extension;

	private $model;

	public function __construct() {
		$this->register_autoload();
		$this->model = $this->get_model('ArticlesModel');
		$this->initialize();
	}

	private function initialize() {
		if(empty($this->get_params()['initialized'])) {
			$this->model->initialize($this->path.DS.'resource'.DS.'initialize.sql');
			$this->set_params(null, [
				'initialized' => 'true'
			]);
		}
	}

	public function get_category($category) {
		return $this->model->get_category($category);
	}

	public function get_article($article) {
		return $this->model->get_article($article);
	}

	/**
	 * @param $fields = ['name', 'category', 'text']
	 * @return mixed
	 */
	public function add_article($fields) {
		return $this->model->add_article($fields);
	}

	public function update_article($article, $fields) {
		$this->model->update_article($article, $fields);
	}

	public function delete_article($article) {
		$this->model->delete_article($article);
	}

	public function delete_category($category) {
		$this->model->delete_category($category);
	}
}