CREATE TABLE IF NOT EXISTS `RU_articles` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(255) UNIQUE NOT NULL DEFAULT '',
  `category` BIGINT NOT NULL DEFAULT 1,
  `text` TEXT NOT NULL DEFAULT '',
  `deleted` TINYINT(1) NOT NULL DEFAULT 0,
  KEY (`name`),
  KEY (`category`)
);

CREATE TABLE IF NOT EXISTS `RU_categories` (
  `id` SERIAL PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL DEFAULT '',
  `deleted` TINYINT(1) NOT NULL DEFAULT 0
);

INSERT INTO `RU_categories` SET `name` = 'без категории'