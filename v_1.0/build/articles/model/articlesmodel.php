<?php

class ArticlesModel extends SuperModel {

	private static $articles_cache;

	public function initialize($file) {
        if(is_file($file)) {
			$sql    = file_get_contents($file);
			$sql_chunks = explode("\n\n", $sql);
			foreach($sql_chunks as $el) {
				$this->execute($el);
			}
		}
    }

	public function get_category($category) {
		$sql = "select * from {$this->current_language}_articles";
		$sql .= " where `category` = (select id from {$this->current_language}_categories";
		$sql .= ' where name = ?) and `deleted` = 0';
		$sth = $this->db->prepare($sql);
		$sth->bindParam(1, $category, PDO::PARAM_STR);
		$sth->execute();
		return $this->get_arrays_from_statement($sth);
	}

	public function get_article($article) {
		if(!empty(static::$articles_cache[(int)$article])) {
			return static::$articles_cache[(int)$article];
		} else {
			$sql = "select * from {$this->current_language}_articles";
			$sql .= ' where (`id` = ? or `name` = ?) and `deleted` = 0';
			$sth = $this->db->prepare($sql);
			$sth->bindParam(1, $article, PDO::PARAM_INT);
			$sth->bindParam(2, $article, PDO::PARAM_STR);
			$sth->execute();
			$result = $this->get_array_from_statement($sth);
			static::$articles_cache[$result['id']] = $result;
			return $result;
		}
	}

	public function add_article($fields) {
		$table = $this->current_language.'_articles';
		$params = [
			'fields' => $fields
		];
		if(!empty($fields['category']) && !is_int($fields['category'])) {
			$fields['category'] = $this->select($this->current_language.'_categories', [
				'fields' => ['id'],
				'where' => 'name = '.$this->db->quote($fields['category'])
			]);
		}
		$this->validate_articles_fields($params['fields']);
		$params['fields'] = array_merge($params['fields'], $fields);

		return $this->insert($table, $params);
	}

	public function update_article($article, $fields) {
		$table = $this->current_language.'_articles';
		$where = 'id = '.((int)$article).' or name = \''.strval($article).'\'';
		if(!empty($fields['category']) && !is_int($fields['category'])) {
			$fields['category'] = $this->select($this->current_language.'_categories', [
				'fields' => ['id'],
				'where' => 'name = '.$this->db->quote($fields['category'])
			]);
		}
		$params = [
			'fields' => $fields,
			'where' => $where
		];
		$this->update($table, $params);
	}

	private function validate_articles_fields($fields) {
		if(empty($fields['name'])) {
			throw new InvalidArgumentException('you need to specify article name');
		}
		if(empty($fields['text'])) {
			throw new InvalidArgumentException('you need to specify article text');
		}
	}

	public function delete_article($article) {
		$table = $this->current_language.'_articles';
		$sql = "update {$table} set `deleted` = 1";
		$sql .= ' where `id` = ? or `name` = ?';
		$sth = $this->db->prepare($sql);
		$sth->bindParam(1, $article, PDO::PARAM_INT);
		$sth->bindParam(2, $article, PDO::PARAM_STR);
		$sth->execute();
	}

	public function delete_category($category) {
		$table = $this->current_language.'_articles';
		$sql = "update {$table} set `deleted` = 1";
		$sql .= " where `category` = (select id from {$this->current_language}_categories";
		$sql .= ' where name = ?) and `deleted` = 0';
		$sth = $this->db->prepare($sql);
		$sth->bindParam(1, $category, PDO::PARAM_STR);
		$sth->execute();
	}
}