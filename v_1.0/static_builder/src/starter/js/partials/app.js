(function(window) {
    'use strict';
    window.App = {};
    _.extend(App, Backbone.Events, {

        max_images_count : 100,
        max_filesize : 1024*1024,
        post_max_size : 1024*1024*8,
        max_file_uploads : 20,

        start: function() {
            this.register_events();

            $.ajaxSetup({
                beforeSend  : function() {
                    this.url += (this.url.indexOf('?') > -1 ? '&' : '?') + 'ajax=1';
                    var data = this.data || '';
                    var get_params = App.parse_url_params(this.url.split('?')[1]);
                    var post_params = App.parse_url_params(data);
                    this.url += '&token='+App.get_token(get_params, post_params);
                },
                error : function(xhr,status, error) {
                    NotificationView.display(LanguageModel.get('request_error'), 'error');
                }
            });

            this.set_title();
        },

        set_title: function() {
            this.object_loaded('LanguageModel')
                .then(function() {
                    var set_title = function() {
                        var title = LanguageModel.get('titles')[location.pathname];
                        if(title) {
                            $('title').html(title);
                            return true;
                        }
                        return false;
                    };
                    if(!set_title()) {
                        LanguageModel.on('sync', set_title);
                    }
                    App.on('Page:loaded', set_title);
                })
        },

        register_events: function() {
            $(document).on('click', 'a[href]:not(.external)', function(e) {
                var href = $(this).attr('href');
                if(href.slice(-1) == '/') {
                    href = href.slice(0,-1);
                }
                if(href.indexOf('http') == -1 && href.indexOf('www') == -1 && href.indexOf('javascript') == -1) {
                    e.preventDefault();
                    App.router.navigate(href, {trigger:true});
                }
            });

            $(document).on('submit', 'form', function(e) {
                e.preventDefault();
            });
        },

        get_cookie : function(name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
              ));
            return matches ? decodeURIComponent(matches[1]) : undefined;
        },

        set_cookie : function(name, value, options) {
              options = options || {};

              var expires = options.expires;

              if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires*1000);
                expires = options.expires = d;
              }
              if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
              }

              value = encodeURIComponent(value);

              var updatedCookie = name + "=" + value;

              for(var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                  updatedCookie += "=" + propValue;
                 }
              }

              document.cookie = updatedCookie;
        },

        delete_cookie : function(name) {
            this.set_cookie(name, "", { expires: -1 })
        },

        go_to: function(url, options) {
            options = options || {trigger : true};
            App.router.navigate(url, options);
        },

        get_token: function(get_params, post_params) {
            var str_to_hash = (App.get_cookie('user') || '') +
                (App.get_cookie('pfm_session_id') || '')
                + JSON.stringify($.extend(get_params, post_params));
            return CryptoJS.MD5(str_to_hash).toString();
        },

        parse_url_params: function(url) {
            // http://stackoverflow.com/a/23946023/2407309
            if (typeof url == 'undefined') {
                url = window.location.search
            } else if(typeof url !== 'string') {
                return {};
            }
            var url = url.split('#')[0]; // Discard fragment identifier.
            var urlParams = {};
            var queryString = url.split('?')[1];
            if (!queryString) {
                if (url.search('=') !== false) {
                    queryString = url
                }
            }
            if (queryString) {
                var keyValuePairs = queryString.split('&');
                for (var i = 0; i < keyValuePairs.length; i++) {
                    var keyValuePair = keyValuePairs[i].split('=');
                    var paramName = keyValuePair[0];
                    var paramValue = keyValuePair[1] || '';
                    urlParams[paramName] = decodeURIComponent(paramValue.replace(/\+/g, ' '))
                }
            }
            return urlParams
        },

        upload_images : function(files, url, callback) {
            var images = [], image;
            var image_exceed_error = _.once(function() {
                var msg = LanguageModel.get('max_file_size');
                msg += ' '+App.bytes_to_string(App.max_filesize);
                NotificationView.display(msg);
            });
            var no_image_error = _.once(function() {
                NotificationView.display(LanguageModel.get('file_must_be_image'), 'error');
            });
            if(files && files.length) {
                $.each(files, function(k,v) {
                    if(v.type.split('/')[0] == 'image') {
                        if(v.size < App.max_filesize) {
                            images.push(v);
                        } else {
                            image_exceed_error();
                        }
                    } else {
                        no_image_error();
                    }
                });
                var FormDataWithLength = function() {
                    this.length = 0;
                    this.form_data = new FormData();
                };
                FormDataWithLength.prototype.append = function(key, value) {
                    this.length++;
                    this.form_data.append(key, value);
                };
                FormDataWithLength.prototype.get_data = function() {
                    return this.form_data;
                };
                var form_data = new FormDataWithLength();
                var current_size = 0, counter = 0, files_count = 0;
                var max_size = App.post_max_size-App.max_filesize;
                var forms = [];
                while((image = images.pop()) && (files_count < App.max_images_count)) {
                    if(current_size < max_size && counter < App.max_file_uploads) {
                        current_size += image.size;
                        form_data.append('images[]', image);
                    } else {
                        forms.push(form_data);
                        form_data = new FormDataWithLength();
                        form_data.append('images[]', image);
                        counter = 0;
                        current_size = 0;
                    }
                    files_count++;
                    counter++;
                }
                if(form_data.length) {
                    forms.push(form_data);
                }
                var promise = new Promise(function(resolve){
                    resolve();
                });
                var post;
                return forms.reduce(function(promise, form) {
                    return promise.then(function() {
                        Pace.track(function() {
                            try {
                                post = $.ajax({
                                    url : url,
                                    data : form.get_data(),
                                    type : 'post',
                                    cache : false,
                                    processData : false,
                                    contentType : false,
                                    dataType : 'json',
                                    success : function(data) {
                                        if(typeof callback == 'function') {
                                            callback(data);
                                        }
                                    }
                                });
                            } catch (err) {
                                console.log(err);
                            }
                        });
                        return post;
                    })
                }, promise);
            } else {
                return new Promise(function(resolve) {
                    resolve();
                });
            }
        },

        bytes_to_string : function(bytes) {
            bytes = String(bytes);
            switch (bytes.length) {
                case 7:
                    return bytes[0]+'MB';
                case 10:
                    return bytes[0]+'GB';
                case 13:
                    return bytes[0]+'TB';
                default:
                    return 'oO';
            }
        },

        object_loaded: function(name) {
            return new Promise(function(resolve, reject) {
                var counter = 0;
                var interval = setInterval(function() {
                    if(window[name]) {
                        clearInterval(interval);
                        resolve();
                    }
                    if(++counter > 500) {
                        clearInterval(interval);
                        reject();
                    }
                }, 100);
            });
        }
    });
    App.start();
})(window);
