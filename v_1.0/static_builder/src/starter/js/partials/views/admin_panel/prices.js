(function() {
    window.PricesView = Backbone.View.extend({
        el: '#prices',

        initialize: function() {
            this.initialize_editable();
        },

        initialize_editable: function() {
            var rows = this.$el.find('tr');
            rows.each(function() {
                $(this).find('#name').editable();
                $(this).find('#file').editable();
            });
        },

        events: {
            'click #toggle-add-form': 'toggle_add_form',
            'submit #add-price-form': 'add',
            'click #delete-price': 'delete_price'
        },

        toggle_add_form: function() {
            this.$el.find('#add-price-form').toggleClass('hide');
        },

        add: function(e) {
            var form = $(e.target);
            $.ajax({
                url: form.attr('action'),
                data: new FormData(form.get(0)),
                dataType: 'json',
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(response) {
                    NotificationView.display(response.message, response.status);
                    if(response.status == 'success') {
                        App.router.reload();
                    }
                }
            })
        },

        delete_price: function(e) {
            if(confirm(LanguageModel.get('confirm_delete_price'))) {
                var el = $(e.target);
                var params = {
                    id: el.data('id')
                };
                $.post('/admin_panel/delete_price', params, function(response) {
                    NotificationView.display(response.message, response.status);
                    if(response.status == 'success') {
                        el.parents('tr').remove();
                    }
                }, 'json')
            }
        }
    });
})();
