(function() {
    'use strict';

    window.AtticStairsView = $.extend(true, SuperView, {
        initialize: function() {
            App.object_loaded('CKEDITOR')
                .then(function() {
                    CKEDITOR.replace('article', {
                        removePlugins: 'inlinesave,save,sourcedialog'
                    });
                });
        },

        events: {
            'change [type=file]': 'show_preview',
            'submit': 'submit_handler'
        },

        show_preview: function(e) {
            var input = e.target;
            var wrapper = $(input).parent();
            if(wrapper.find('img')) {
                wrapper.find('img').remove();
            }
            var file = input.files[0];
            var reader, img;
            if(file.size < App.max_filesize) {
                if(file.type.split('/')[0] == 'image') {
                    reader = new FileReader();
                    reader.onload = function(f) {
                        img = new Image();
                        img.src = f.target.result;
                        $(img).css({
                           'max-width': '160px',
                            'max-height': '160px'
                        });
                        wrapper.append(img);
                    };
                    reader.readAsDataURL(file);
                }
            } else {
                var msg = LanguageModel.get('max_file_size');
                msg += ' '+App.bytes_to_string(App.max_filesize);
                NotificationView.display(msg);
            }
        },

        submit_handler: function(e) {
            CKEDITOR.instances.article.updateElement();
            var table = e.target;
            $.ajax({
                url: table.action,
                data: new FormData(table),
                type: 'post',
                success: function(response) {
                    NotificationView.display(response.message, response.status);
                    if(response.status == 'success') {
                        history.back();
                    }
                },
                processData: false,
                contentType : false,
                dataType : 'json'
            })
        }
    });
})();