(function() {
    'use strict';
    var EditAlbumView = AlbumView.extend({
        el: '#edit-album',

        initialize: function() {
            this.images_to_delete = [];
            this.files_queue = [];
        },

        events: {
            'click #remove-img': 'remove_img',
            'click #make-cover': 'change_cover'
        },

        remove_img: function(e) {
            var el = $(e.target);
            var name = el.parents('.thumbnail').find('img').attr('src').split('/').slice(-1)[0]
                .split('?')[0];
            this.images_to_delete.push(name);
            el.parents('.thumbnail').remove();
        },

        change_cover: function(e) {
            var el = $(e.target);
            var gallery = el.data('gallery');
            var album = el.data('album');
            var new_cover = el.parents('.thumbnail').find('img').attr('src').split('/')
                .slice(-1)[0].split('?')[0];
            var params = {
                new_cover: new_cover
            };
            $.post('/admin_panel/gallery/'+gallery+'/'+album+'/change_cover', params, function(json){
                NotificationView.display(json.message, json.status);
            }, 'json');
        },

        save: function() {
            var $this = this;
            var gallery = this.$el.find('[name=gallery]').val();
            var album = this.$el.find('[name=album]').val();
            var delete_images_params = {
                images: this.images_to_delete
            };
            var thumbs = this.$el.find('.thumbnail');
            if(thumbs.length == 0 && this.files_queue.length == 0) {
                var remove_album_params = {
                    gallery: gallery,
                    album: album
                };
                $.post('/admin_panel/remove_gallery_album', remove_album_params, function() {
                    history.back();
                })
            } else {
                var remove_images_url = '/admin_panel/gallery/'+gallery+'/'+album+'/remove_images';
                if(this.files_queue.length && this.images_to_delete.length) {
                    $.when(App.upload_images(this.files_queue, this.$el.attr('action')),
                            $.post(remove_images_url, delete_images_params))
                        .then(function() {
                            NotificationView.display(LanguageModel.get('success'), 'success');
                            $this.files_queue = [];
                            $this.images_to_delete = [];
                        });
                } else if(this.files_queue.length) {
                    App.upload_images(this.files_queue, this.$el.attr('action'))
                        .then(function() {
                            NotificationView.display(LanguageModel.get('success'), 'success');
                            $this.files_queue = [];
                        })
                } else if(this.images_to_delete.length) {
                    $.post(remove_images_url, delete_images_params, function() {
                        NotificationView.display(LanguageModel.get('success'), 'success');
                        $this.images_to_delete = [];
                    })
                }
            }
        }
    });
    window.EditAlbumView = Backbone.View.extend(EditAlbumView);
})();