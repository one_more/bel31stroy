(function() {
    'use strict';
    window.GalleryView = Backbone.View.extend({
        el: '#gallery',

        events: {
            "click #remove-album": 'remove_album'
        },

        remove_album: function(e) {
            e.preventDefault();
            e.stopPropagation();
            if(confirm(LanguageModel.get('confirm_delete_images'))) {
                var el = $(e.target);
                var params = {
                    gallery: el.data('gallery'),
                    album: el.data('album')
                };
                $.post('/admin_panel/remove_gallery_album', params, function(json) {
                    NotificationView.display(json.message, json.status);
                    if(json.status == 'success') {
                        el.parents('.thumbnail').parent().remove();
                    }
                }, 'json');
            }
        }
    });
})();
