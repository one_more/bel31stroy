(function() {
    'use strict';

    window.SuperView = {
        extend: function(view) {
            var new_obj = $.extend(true, view, this);
            for(var i in new_obj) {
                if(typeof this[i] == 'function' && new_obj[i]) {
                    new_obj[i].bind(new_obj);
                }
            }
            return new_obj;
        }
    };
})();