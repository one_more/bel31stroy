(function() {
    'use strict';

    var ext_obj = AtticStairsView.extend({
        el: '#attic-stairs-edit-form'
    });

    window.EditAtticStairsView = Backbone.View.extend(ext_obj);
})();