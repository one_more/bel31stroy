(function() {
    'use strict';

    var ext_obj = AtticStairsView.extend({
        el: '#attic-stairs-add-form'
    });

    window.AddAtticStairsView = Backbone.View.extend(ext_obj);
})();