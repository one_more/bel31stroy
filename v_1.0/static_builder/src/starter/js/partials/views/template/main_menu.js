(function() {
    'use strict';

    window.MainMenuView = Backbone.View.extend({
        el: '.sf-menu',

        initialize: function() {
            this.$el.find('[href="'+location.pathname+'"]')
                .parents('li')
                .addClass('current');
        },

        events: {
            'click li': 'change_active'
        },

        change_active: function(e) {
            var el = $(e.currentTarget);
            if(!el.hasClass('current')) {
                this.$el.find('li.current').removeClass('current');
                el.addClass('current');
            }
        }
    });
})();
