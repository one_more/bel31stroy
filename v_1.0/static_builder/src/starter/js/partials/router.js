(function() {
    'use strict';
    App.router = Backbone.Router.extend({

        initialize: function() {
            this.init_views();
        },

        init_views: function() {
            var $this = this;
            if(typeof this['global_views'] == 'undefined') {
                var main_menu_promise = App.object_loaded('MainMenuView');
                main_menu_promise.then(function() {
                    $this.global_views = [new MainMenuView];
                });
            }
            switch(this.current().route) {
                case 'add_album':
                    App.object_loaded('AddAlbumView')
                        .then(function() {
                            $this.views = [new AddAlbumView];
                        });
                    break;
                case 'gallery':
                    App.object_loaded('GalleryView')
                        .then(function() {
                            $this.views = [new GalleryView];
                        });
                    break;
                case 'edit_album':
                    App.object_loaded('EditAlbumView')
                        .then(function() {
                            $this.views = [new EditAlbumView];
                        });
                    break;
                case 'prices':
                    App.object_loaded('EditAlbumView')
                        .then(function() {
                            $this.views = [new PricesView];
                        });
                    break;
                case 'attic_stairs_table':
                    App.object_loaded('AtticStairsTableView')
                        .then(function() {
                            $this.views = [new AtticStairsTableView];
                        });
                    break;
                case 'add_attic_stairs':
                    App.object_loaded('AddAtticStairsView')
                        .then(function() {
                            $this.views = [new AddAtticStairsView];
                        });
                    break;
                case 'edit_attic_stairs':
                    App.object_loaded('EditAtticStairsView')
                        .then(function() {
                            $this.views = [new EditAtticStairsView];
                        });
                    break;
            }
        },

        current: function() {
            var Router = this,
                fragment = location.pathname.slice(1),
                routes = _.pairs(Router.routes),
                route = null, params = null, matched;

            matched = _.find(routes, function(handler) {
                route = _.isRegExp(handler[0]) ? handler[0] : Router._routeToRegExp(handler[0]);
                return route.test(fragment);
            });

            if(matched) {
                params = Router._extractParameters(route, fragment);
                route = matched[1];
            }

            return {
                route : route,
                fragment : fragment,
                params : params
            };
        },

        routes: {
            'admin_panel/edit_user/:id': 'load_positions',
            'admin_panel': 'load_positions',
            'admin_panel/add_user': 'load_positions',
            'admin_panel/gallery/:name/add_album': 'add_album',
            'admin_panel/gallery/:name': 'gallery',
            'admin_panel/gallery/:gallery/:album': 'edit_album',
            'admin_panel/prices': 'prices',
            'admin_panel/attic_stairs/:category': 'attic_stairs_table',
            'admin_panel/attic_stairs/:category/add_form': 'add_attic_stairs',
            'admin_panel/attic_stairs/:category/edit_form/:id': 'edit_attic_stairs',
            '': 'load_positions',
            'our_objects': 'load_positions',
            'constructions/ladders': 'load_positions',
            'constructions/roofing': 'load_positions',
            'constructions/windows': 'load_positions',
            'constructions/attic_stairs/facro': 'load_positions',
            'album/:gallery/:album': 'load_positions',
            'constructions/attic_stairs/:category': 'load_positions',
            'constructions/attic_stairs_oman': 'load_positions',
            'constructions/attic_stairs/:category/:id': 'load_positions',
            'contacts': 'load_positions'
        },

        add_album: function() {
            this.load_positions();
        },

        gallery: function() {
            this.load_positions();
        },

        edit_album: function() {
            this.load_positions();
        },

        prices: function() {
            this.load_positions();
        },

        attic_stairs_table: function() {
            this.load_positions();
        },

        add_attic_stairs: function() {
            this.load_positions();
        },

        edit_attic_stairs: function() {
            this.load_positions();
        },

        load_positions: function(url) {
            var $this = this;
            url = url || location.pathname;
            var ajax = $.post(location.pathname, {}, function(data) {
                for(var i in data) {
                    $('#'+i).html(data[i]);
                     App.trigger('Page:loaded', {
                        page: location.pathname.split('/').slice(-1)[0]
                    });
                }
            }, 'json');
            ajax.then(function() {
                $this.init_views();
            });
            return ajax;
        },

        reload: function() {
            this.load_positions();
        }
    });
    App.router = new App.router;
    Backbone.history.start({pushState: true, silent: true});
})();
