<?php

namespace common\mappers;

use common\adapters\MysqlAdapter;
use common\collections\BaseCollection;
use common\models\PriceModel;

/**
 * Class PricesMapper
 * @package common\mappers
 *
 * @property MysqlAdapter $adapter
 */
class PricesMapper extends BaseMapper {

    /**
     * @return MysqlAdapter
     */
    public function get_adapter() {
        return new MysqlAdapter(CURRENT_LANG.'_prices');
    }

    /**
     * @return BaseCollection
     */
    public function get_all() {
        $collection = new BaseCollection(PriceModel::class);
        $collection->load($this->adapter->select()->execute()->get_arrays());
        return $collection;
    }
}