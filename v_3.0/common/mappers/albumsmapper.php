<?php

namespace common\mappers;

use common\collections\BaseCollection;
use common\models\AlbumModel;
use common\models\PagingModel;

class AlbumsMapper extends BaseMapper {

    public function get_adapter() {
        return null;
    }

    /**
     * @param $gallery_name
     * @return BaseCollection
     */
    public function find_by_gallery_name($gallery_name) {
        $collection = new BaseCollection(AlbumModel::class);
        $collection->load($this->get_albums($gallery_name));
        return $collection;
    }

    /**
     * @param $name
     * @return AlbumModel
     */
    public function find_by_name($name) {
        $galleries = glob(WEB_ROOT.DS.'images'.DS.'galleries'.DS.'*');
        foreach($galleries as $gallery) {
            $albums = glob($gallery.DS.'*');
            foreach($albums as $album) {
                if(basename($album) == $name) {
                    return new AlbumModel($this->get_album($album));
                }
            }
        }
        return new AlbumModel();
    }

    public function get_page($gallery_name, $page = 1, $per_page = 8) {
        $collection = new BaseCollection(AlbumModel::class);
        $offset = ($page-1)*$per_page;
        $collection->load($this->get_albums($gallery_name, $offset, $per_page));
        return $collection;
    }

    public function get_paging($gallery_name, $page = 1, $per_page = 8) {
        $path = WEB_ROOT.DS.'images'.DS.'galleries'.DS.$gallery_name.DS.'*';
        $albums_count = count(glob($path));
        return new PagingModel([
            'current' => $page,
            'pages' => ceil($albums_count / $per_page)
        ]);
    }

    private function get_albums($gallery_name, $offset = null, $limit = null) {
        $albums = glob(WEB_ROOT.DS.'images'.DS.'galleries'.DS.$gallery_name.DS.'*');
        $albums = array_slice($albums, $offset, $limit);
        return array_map([$this, 'get_album'], $albums);
    }

    private function get_album($album) {
        $album_name = basename($album);
        $album = array_map(function($image) {
            return str_replace(WEB_ROOT, '', $image);
        }, glob($album.DS.'*'));
        $cover = array_filter($album, function($image) {
            return strpos($image, 'cover') !== false;
        });
        if(count($cover)) {
            $cover = reset($cover);
        } else {
            $cover = reset($album);
        }
        return [
            'name' => $album_name,
            'images' => $album,
            'cover' => $cover
        ];
    }
}