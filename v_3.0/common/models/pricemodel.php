<?php

namespace common\models;

/**
 * Class PriceModel
 * @package common\models
 *
 * @property int $id
 * @property string $name
 * @property string $file
 */
class PriceModel extends BaseModel {

    protected $fields = [
        'id' => null,
        'name' => null,
        'file' => null
    ];
}