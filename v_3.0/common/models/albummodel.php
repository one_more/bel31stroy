<?php

namespace common\models;

/**
 * Class AlbumModel
 * @package common\models
 *
 * @property string $name
 * @property array $images
 * @property string $cover
 */
class AlbumModel extends BaseModel {

    protected $fields = [
        'name' => null,
        'images' => [],
        'cover' => null
    ];
}