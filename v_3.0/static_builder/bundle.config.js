var lazypipe = require('lazypipe'),
    babel = require('gulp-babel'),
    stylus = require('gulp-stylus'),
    nib = require('nib'),
    cssnext = require('gulp-cssnext'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    cssmin = require('gulp-cssmin');

var default_options = {
    useMin: true,
    uglify: false,
    minCSS: false,
    transforms: {
        scripts: lazypipe().pipe(babel).pipe(uglify),
        styles: lazypipe().pipe(stylus).pipe(prefixer).pipe(cssmin).pipe(cssnext)
    },
    pluginOptions: {
        "gulp-stylus": {use: nib()},
        "gulp-cssnext": {compress: true}
    },
    watch: {
        scripts: true,
        styles: true
    }
};

var vendor_js = [
    './src/vendor/jquery-2.1.3.js',
    './src/vendor/underscore.js',
    './src/vendor/backbone.js',
    './src/vendor/crypto/md5.js',
    './src/vendor/es6-shim.js',
    './src/vendor/smart.core.min.js',
    './src/vendor/pace/pace.min.js'
];

var vendor_css = [
    './src/vendor/bootstrap/bootstrap.css',
    './src/vendor/bootstrap/bootstrap-theme.css',
    './src/vendor/icomoon.css',
    './src/vendor/pace/pace.css'
];

module.exports = {
    bundle: {
        vendor: {
            scripts: vendor_js,
            styles: vendor_css,
            options: {
                useMin: true,
                uglify: false,
                minCSS: false,
                transforms: {
                    scripts: lazypipe().pipe(uglify),
                    styles: lazypipe().pipe(cssmin)
                },
                watch: {
                    scripts: false,
                    styles: false
                }
            }
        },
        starter_vendor: {
            scripts: [
                './src/vendor/template/jquery.ui.totop.js',
                './src/vendor/template/superfish.js',
                './src/vendor/template/jquery.hoverIntent.minified.js',
                './src/vendor/template/jquery.responsivemenu.js',
                './src/vendor/template/jquery.mobilemenu.js',
                './src/vendor/template/jquery.easing.1.3.js',
                './src/vendor/template/jquery.elastislide.js',
                './src/vendor/template/mobile.js',
                './src/vendor/template/jquery.flexslider.js',
                './src/vendor/grid_gallery/modernizr.custom.js',
                './src/vendor/grid_gallery/classie.js',
                './src/vendor/grid_gallery/imagesloaded.pkgd.min.js',
                './src/vendor/grid_gallery/masonry.pkgd.min.js',
                './src/vendor/grid_gallery/cbpGridGallery.js',
                './src/vendor/pace/pace.min.js'
            ]
        },
        starter: {
            scripts: [
                './src/global/js/**/*.js',
                './src/starter/js/**/*.js'
            ],
            styles: [
                './src/vendor/icomoon.css',
                './src/vendor/pace/pace.css',
                './src/starter/styles/**/*.styl',
                './src/starter/styles/**/*.css',
                './src/global/styles/**/*.styl',
            ],
            options: default_options
        },
        "admin_panel": {
            scripts: [
                './src/vendor/bootstrap/bootstrap.js',
                './src/global/js/**/*.js',
                './src/admin_panel/js/**/*.js'
            ],
            styles: [
                './src/admin_panel/styles/**/*.styl',
                './src/admin_panel/styles/**/*.css',
                './src/global/styles/**/*.styl'
            ],
            options: default_options
        },
        tools: {
            scripts: [
                './src/global/js/**/*.js',
                './src/tools/js/**/*.js'
            ],
            styles: [
                './src/tools/styles/**/*.styl',
                './src/global/styles/**/*.styl'
            ],
            options: default_options
        }
    }
};
