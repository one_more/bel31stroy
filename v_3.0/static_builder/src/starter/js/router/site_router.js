(function () {
    'use strict';

    let SiteRouter = Backbone.Router.extend(BaseRouter.extend({
        routes: {
            '': 'index',
            'our_objects': 'gallery',
            'contacts': 'contacts',
            'constructions/ladders': 'gallery',
            'constructions/roofing': 'gallery',
            'constructions/windows': 'windows_page',
            'constructions/attic_stairs/:category': 'attic_stairs',
            'constructions/attic_stairs/:category/:id': 'attic_stair',
            'constructions/attic_stairs_oman': 'attic_stairs_oman',
            'album/:gallery_name/:album_name': 'album'
        },

        init_views: function () {
            let views = ['MainMenuView'];
            Helpers.objects_loaded(views).then(() => {
                this.views = [
                    new MainMenuView
                ];
            })
        },

        index: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new MainPageView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        },
        
        gallery: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new GalleryView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        },

        windows_page: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new WindowsPageView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        },

        attic_stairs: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new AtticStairsView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        },

        attic_stair: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new AtticStairView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        },

        attic_stairs_oman: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new ListImagesView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        },

        contacts: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new ContactsPageView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        },
        
        album: function () {
            (window.templates = new TemplatesCollection(), templates.on('sync', () => {
                $('[data-block="main"]').html(new GalleryAlbumView().render());
                App.trigger('Page:loaded');
            }), templates.fetch());
        }
    }));

    window.Router = new SiteRouter;
})();