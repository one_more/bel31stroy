_.defer(() => {
    "use strict";

    window.PricesView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'PricesView'
    }))
});