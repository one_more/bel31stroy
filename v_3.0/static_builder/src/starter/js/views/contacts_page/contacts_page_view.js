_.defer(() => {
    "use strict";

    window.ContactsPageView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'ContactsPageView'
    }))
});