_.defer(() => {
    "use strict";

    window.AtticStairsView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'AtticStairsView'
    }))
});