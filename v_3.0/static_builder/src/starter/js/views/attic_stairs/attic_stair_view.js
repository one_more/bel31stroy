_.defer(() => {
    "use strict";

    window.AtticStairView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'AtticStairView'
    }))
});