_.defer(() => {
    "use strict";

    window.ListImagesView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'ListImagesView'
    }))
});