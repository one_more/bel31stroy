_.defer(() => {
    "use strict";

    window.MainPageView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'MainPageView',

        get_data: function () {
            return {
                slider: new FlexSliderView().render().html(),
                carousel: new ElastiSlideView().render().html(),
                prices_list: new PricesView().render().html()
            }
        }
    }))
});