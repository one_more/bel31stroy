_.defer(() => {
    "use strict";

    window.GalleryView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'GalleryView'
    }))
});