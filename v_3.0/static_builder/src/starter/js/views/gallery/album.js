_.defer(() => {
    "use strict";

    window.GalleryAlbumView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'GalleryAlbumView'
    }))
});