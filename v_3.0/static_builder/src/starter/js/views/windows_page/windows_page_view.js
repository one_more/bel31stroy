_.defer(() => {
    "use strict";

    window.WindowsPageView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'WindowsPageView',

        get_data: () => {
            return {
                gallery: new GalleryView().render().html(),
                images_list: new ListImagesView().render().html()
            }
        }
    }))
});