_.defer(() => {
    "use strict";

    window.FlexSliderView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'FlexSliderView'
    }))
});