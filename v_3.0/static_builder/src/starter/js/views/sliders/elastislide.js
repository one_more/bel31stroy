_.defer(() => {
    "use strict";

    window.ElastiSlideView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'ElastiSlideView'
    }))
});