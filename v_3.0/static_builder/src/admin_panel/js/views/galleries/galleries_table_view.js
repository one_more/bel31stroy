_.defer(() => {
    "use strict";

    window.GalleriesTableView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'GalleriesTableView'
    }))
});