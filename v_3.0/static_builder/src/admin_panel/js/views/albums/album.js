(() => {
    'use strict';

    window.AlbumView = {

        events: {
            'click button': function(event) {
                event.preventDefault();
            },
            'submit': 'save',
            'click #select-photo': 'select_photo',
            'change [name=images]': 'preview_album'
        },

        select_photo: function(event) {
            event.preventDefault();
            this.$el.find('[name=images]').trigger('click');
        },

        preview_album: function(event) {
            var $this = this;
            var input = event.target;
            var files = input.files;
            var reader, img;
            var thumbnail, caption, btn;
            if(files.length) {
                [].forEach.call(files, function(file) {
                    if(file.size < App.max_filesize) {
                        if(file.type.split('/')[0] == 'image') {
                            reader = new FileReader();
                            reader.onload = function(f) {
                                img = new Image();
                                img.src = f.target.result;
                                thumbnail = $('<div>', {
                                    'class': 'thumbnail display-inline-block'
                                });
                                img.style.width = '150px';
                                img.style.height = '110px';
                                thumbnail.append(img);
                                caption = document.createElement('div');
                                caption.className = 'caption';
                                btn = document.createElement('button');
                                btn.className = 'btn btn-danger delete-thumbnail btn-xs';
                                btn.innerText = LanguageModel.get('delete');
                                btn.onclick = function(file) {
                                    return function(event) {
                                        var index = _.findIndex($this.files_queue, function(el) {
                                            return _.isEqual(el, file);
                                        });
                                        $this.files_queue.splice(index, 1);
                                        var el = $(event.target);
                                        el.parents('.thumbnail').remove();
                                        $this.display_files_names();
                                    };
                                }(file);
                                caption.appendChild(btn);
                                thumbnail.append(caption);
                                $this.$el.find('#thumbnails').append(thumbnail);
                            };
                            reader.readAsDataURL(file);
                            $this.files_queue.push(file);
                        }
                    } else {
                        var msg = LanguageModel.get('max_file_size');
                        msg += ' '+App.bytes_to_string(App.max_filesize);
                        NotificationView.display(msg);
                    }
                });
            }
            $this.display_files_names();
        },

        display_files_names: function() {
            var img_names = this.files_queue.map(function(el) {
                return el.name;
            });
            this.$el.find('#photos-names').val(img_names.join(','));
        },

        extend: function(view) {
            var new_obj = $.extend(true, view, this);
            for(var i in new_obj) {
                if(typeof this[i] == 'function' && new_obj[i]) {
                    new_obj[i].bind(new_obj);
                }
            }
            return new_obj;
        }
    }
});
