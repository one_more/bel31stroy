_.defer(() => {
    'use strict';

    var AddAlbumViewOld = {
        el: '#add-album',

        initialize: function() {
            this.files_queue = [];
        },

        save: function() {
            var $this = this;
            if(this.files_queue.length) {
                App.upload_images(this.files_queue, this.$el.attr('action'))
                    .then(function(json) {
                        NotificationView.display(json.message, json.status);
                        $this.files_queue = [];
                    })
            } else {
                NotificationView.display(LanguageModel.get('select_images'), 'error');
            }
        }
    };
    window.AddAlbumView = Backbone.View.extend(BaseView.extend({
        tagName: 'div',

        name: 'AddAlbumView'
    }));
});