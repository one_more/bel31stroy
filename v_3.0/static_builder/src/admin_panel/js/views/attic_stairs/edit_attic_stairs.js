(() => {
    'use strict';

    window.EditAtticStairsView = Backbone.View.extend(AtticStairsView.extend({
        el: '#attic-stairs-edit-form'
    }));
});