(() => {
    'use strict';

    window.AtticStairsTableView = Backbone.View.extend({
        el: '#attic-stairs-table',

        events: {
            'click #delete-btn': 'delete_stair'
        },

        delete_stair: function(e) {
            if(confirm(LanguageModel.get('confirm_delete'))) {
                var el = $(e.target);
                $.post(location.pathname+'/delete/'+el.data('param'), {}, function(response) {
                    NotificationView.display(response.message, response.status);
                    if(response.status == 'success') {
                        el.parents('tr').remove();
                    }
                });
            }
        }
    })
});