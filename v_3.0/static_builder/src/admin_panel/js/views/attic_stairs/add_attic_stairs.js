(() => {
    'use strict';

    window.AddAtticStairsView = Backbone.View.extend(AtticStairsView.extend({
        el: '#attic-stairs-add-form'
    }));
});