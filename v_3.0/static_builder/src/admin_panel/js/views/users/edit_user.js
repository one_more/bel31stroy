_.defer(function() {
    'use strict';
    window.EditUserView = Backbone.View.extend(UserFormView.extend({
        tagName: 'div',

        name: 'EditUserView'
    }));
});
