_.defer(function() {
    'use strict';
    window.AddUserView = Backbone.View.extend(UserFormView.extend({
        tagName: 'div',

        name: 'AddUserView'
    }));
});
