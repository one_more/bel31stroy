(() => {
    "use strict";

    window.BaseView = {

        render: function () {
            let template = templates.findWhere({
                name: this.name
            });
            let data = Object.assign(template.get('data'), this.get_data());
            let tpl = new jSmart(template.get('html'));
            return this.$el.html(tpl.fetch(data));
        },

        get_data: () => {},

        extend: function (obj) {
            return $.extend(true, {}, this, obj);
        }
    }
})();