<?php

namespace Starter\routers;
use common\classes\Application;
use common\classes\GetResponse;
use common\classes\LanguageFile;
use common\classes\Response;
use common\classes\Router;
use common\interfaces\Template;
use common\interfaces\View;
use Starter\views\site\AtticStairsView;
use Starter\views\site\AtticStairView;
use Starter\views\site\ContactsPageView;
use Starter\views\site\GalleryAlbumView;
use Starter\views\site\GalleryView;
use Starter\views\site\ListImagesView;
use Starter\views\site\MainPageView;
use Starter\views\site\WindowsPageView;

/**
 * Class SiteRouter
 * @package Starter\routers
 * @decorate \common\decorators\AnnotationsDecorator
 */
class SiteRouter extends Router {
	use TraitStarterRouter;

	public function __construct() {
		$this->routes = [
			'/' => [$this, 'index', 'no check'],
            '/our_objects' => [$this, 'our_objects', 'no check'],
            '/constructions/ladders' => [$this, 'ladders', 'no check'],
            '/constructions/roofing' => [$this, 'roofing', 'no check'],
            '/constructions/windows' => [$this, 'windows', 'no check'],
            '/album/:string/:string' => [$this, 'album', 'no check'],
            '/constructions/attic_stairs/:string' => [$this, 'attic_stairs', 'no check'],
			'/constructions/attic_stairs_oman' => [$this, 'attic_stairs_oman', 'no check'],
			'/constructions/attic_stairs/:string/:number' => [$this, 'attic_stair', 'no check'],
			'/contacts' => [$this, 'contacts', 'no check'],
            '/*not_found' => [$this, 'not_found', 'no check']
		];
        $this->response = new GetResponse();
	}

	public function index() {
        /**
         * @var $view View
         */
        $view = Application::get_class(MainPageView::class);
        $this->response->blocks['main'] = $view->render();
	}

    public function our_objects() {
        $this->gallery('houses');
    }

    public function ladders() {
        $this->gallery('stairs');
    }

    public function roofing() {
        $this->gallery('roofing');
    }

    public function windows() {
        /**
         * @var $view View
         */
        $view = Application::get_class(WindowsPageView::class);
        $this->response->blocks['main'] = $view->render();
    }

    public function attic_stairs($category) {
        /**
         * @var $view View
         */
        $view = Application::get_class(AtticStairsView::class, [$category]);
        $this->response->blocks['main'] = $view->render();
    }

    public function attic_stairs_oman() {
        $dir = WEB_ROOT.DS.'images'.DS.'attic_stairs_oman';
        /**
         * @var $view View
         */
        $view = Application::get_class(ListImagesView::class, [$dir]);
        $this->response->blocks['main'] = $view->render();
    }

    public function attic_stair($category, $stair_id) {
        /**
         * @var $view View
         */
        $view = Application::get_class(AtticStairView::class, [$stair_id]);
        $this->response->blocks['main'] = $view->render();
    }

    public function gallery($name) {
        /**
         * @var $view View
         */
        $view = Application::get_class(GalleryView::class, [$name]);
        $this->response->blocks['main'] = $view->render();
    }

    public function album($gallery, $album) {
        /**
         * @var $view View
         */
        $view = Application::get_class(GalleryAlbumView::class, [$album]);
        $this->response->blocks['main'] = $view->render();
    }

    public function contacts() {
        /**
         * @var $view View
         */
        $view = Application::get_class(ContactsPageView::class);
        $this->response->blocks['main'] = $view->render();
    }

    public function not_found() {
        Response::redirect('/');
        $this->response->blocks['main'] = '';
    }
}
