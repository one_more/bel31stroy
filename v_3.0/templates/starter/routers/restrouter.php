<?php

namespace Starter\routers;

use common\classes\AjaxResponse;
use common\classes\PageTitle;
use common\classes\Router;
use Starter\views\AdminPanel\AddAlbumView;
use Starter\views\AdminPanel\AddUserView;
use Starter\views\AdminPanel\EditUserView;
use Starter\views\AdminPanel\GalleriesTableView;
use Starter\views\AdminPanel\LeftMenuView;
use Starter\views\AdminPanel\LoginFormView;
use Starter\views\AdminPanel\NavbarView;
use Starter\views\AdminPanel\UsersTableView;
use Starter\views\site\AtticStairsView;
use Starter\views\site\AtticStairView;
use Starter\views\site\ContactsPageView;
use Starter\views\site\ElastiSlideView;
use Starter\views\site\FlexSliderView;
use Starter\views\site\GalleryAlbumView;
use Starter\views\site\GalleryView;
use Starter\views\site\ListImagesView;
use Starter\views\site\MainPageView;
use Starter\views\site\PricesView;
use Starter\views\site\WindowsPageView;

class RestRouter extends Router {

    /**
     * @var AjaxResponse
     */
    private $response;

    public function __construct() {
        $this->routes = [
            '/rest/templates/admin_panel' => [$this, 'admin_panel_index_templates'],
            '/rest/templates/admin_panel/users' => [$this, 'admin_panel_users_templates'],
            '/rest/templates/admin_panel/users/page:number' => [$this, 'admin_panel_users_templates'],
            '/rest/templates/admin_panel/login' => [$this, 'admin_panel_login'],
            '/rest/templates/admin_panel/add_user' => [$this, 'admin_panel_add_user'],
            '/rest/templates/admin_panel/edit_user/:number' => [$this, 'admin_panel_edit_user'],
            '/rest/templates/admin_panel/galleries' => [$this, 'admin_panel_galleries'],
            '/rest/templates/admin_panel/gallery/:string' => [$this, 'admin_panel_gallery'],
            '/rest/templates/admin_panel/gallery/:string/page:number' => [$this, 'admin_panel_gallery'],
            '/rest/templates/admin_panel/gallery/:string/add_album' => [$this, 'admin_panel_add_album'],

            '/rest/templates' => [$this, 'index'],
            '/rest/templates/our_objects' => [$this, 'our_objects'],
            '/rest/templates/album/:string/:string' => [$this, 'album'],
            '/rest/templates/constructions/ladders' => [$this, 'ladders', 'no check'],
            '/rest/templates/constructions/roofing' => [$this, 'roofing', 'no check'],
            '/rest/templates/constructions/windows' => [$this, 'windows', 'no check'],
            '/rest/templates/constructions/attic_stairs/:string' => [$this, 'attic_stairs', 'no check'],
			'/rest/templates/constructions/attic_stairs_oman' => [$this, 'attic_stairs_oman', 'no check'],
			'/rest/templates/constructions/attic_stairs/:string/:number' => [$this, 'attic_stair', 'no check'],
			'/rest/templates/contacts' => [$this, 'contacts', 'no check']
        ];

        $this->response = new AjaxResponse();
    }

    public function admin_panel_index_templates() {
        $this->response->templates = [
            (new UsersTableView())->get_template_model()->to_array(),
            (new LeftMenuView())->get_template_model()->to_array(),
            (new NavbarView())->get_template_model()->to_array()
        ];
    }

    public function admin_panel_users_templates($page = 1) {
        $this->response->templates = [
            (new UsersTableView($page))->get_template_model()->to_array()
        ];
    }

    public function admin_panel_login() {
        $this->response->templates = [
            (new LoginFormView())->get_template_model()->to_array()
        ];
    }

    public function admin_panel_add_user() {
        $this->response->templates = [
            (new AddUserView())->get_template_model()->to_array()
        ];
    }

    public function admin_panel_edit_user($id) {
        $this->response->templates = [
            (new EditUserView($id))->get_template_model()->to_array()
        ];
    }

    public function admin_panel_galleries() {
        $this->response->templates = [
            (new GalleriesTableView())->get_template_model()->to_array()
        ];
    }

    public function admin_panel_gallery($name, $page = 1) {
        $this->response->templates = [
            (new \Starter\views\AdminPanel\GalleryView($name, $page))->get_template_model()->to_array()
        ];
    }

    public function admin_panel_add_album($gallery) {
        $this->response->templates = [
            (new AddAlbumView($gallery))->get_template_model()->to_array()
        ];
    }


    public function index() {
        $this->response->templates = [
            (new MainPageView())->get_template_model()->to_array(),
            (new FlexSliderView())->get_template_model()->to_array(),
            (new ElastiSlideView())->get_template_model()->to_array(),
            (new PricesView())->get_template_model()->to_array(),
        ];
    }

    public function our_objects() {
        $this->response->templates = [
            (new GalleryView('houses'))->get_template_model()->to_array()
        ];
    }

    public function ladders() {
        $this->response->templates = [
            (new GalleryView('stairs'))->get_template_model()->to_array()
        ];
    }

    public function roofing() {
        $this->response->templates = [
            (new GalleryView('roofing'))->get_template_model()->to_array()
        ];
    }

    public function windows() {
        $images_dir = WEB_ROOT.DS.'images'.DS.'windows_facro';
        $this->response->templates = [
            (new WindowsPageView())->get_template_model()->to_array(),
            (new GalleryView('windows'))->get_template_model()->to_array(),
            (new ListImagesView($images_dir))->get_template_model()->to_array()
        ];
    }

    public function attic_stairs($category) {
        $this->response->templates = [
            (new AtticStairsView($category))->get_template_model()->to_array()
        ];
    }

    public function attic_stair($category, $id) {
        $this->response->templates = [
            (new AtticStairView($id))->get_template_model()->to_array()
        ];
    }

    public function attic_stairs_oman() {
        $dir = WEB_ROOT.DS.'images'.DS.'attic_stairs_oman';
        $this->response->templates = [
            (new ListImagesView($dir))->get_template_model()->to_array()
        ];
    }

    public function album($gallery_name, $album_name) {
        $this->response->templates = [
            (new GalleryAlbumView($album_name))->get_template_model()->to_array()
        ];
    }

    public function contacts() {
        $this->response->templates = [
            (new ContactsPageView())->get_template_model()->to_array()
        ];
    }

    public function __destruct() {
        $this->response->title = (string)new PageTitle();
        echo $this->response;
    }
}