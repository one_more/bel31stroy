<?php

namespace Starter\views\AdminPanel;

use common\classes\Application;
use common\mappers\AlbumsMapper;
use common\views\TemplateView;

class GalleryView extends TemplateView {

    private $template_name = 'gallery.tpl.html';
    private $gallery_name;
    private $page;

	public function __construct($gallery, $page = 1) {
		parent::__construct();
		$path = $this->template->get_path();
        $this->setTemplateDir($path.DS.'templates'.DS.'admin_panel'.DS.'galleries');
        $this->addTemplateDir($path.DS.'templates'.DS.'admin_panel'.DS.'common');

        $this->gallery_name = $gallery;
        $this->page = $page;
	}

	public function render() {
        $this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        /**
         * @var $mapper AlbumsMapper
         */
        $mapper = Application::get_class(AlbumsMapper::class);
        $albums = $mapper->get_page($this->gallery_name, $this->page);
        $template_dir = $this->getTemplateDir(1);
        return [
            'gallery_name' => $this->gallery_name,
            'albums' => $albums->to_array(),
            'paging_model' => $mapper->get_paging($this->gallery_name, $this->page)->to_array(),
            'base_url' => "/admin_panel/gallery/{$this->gallery_name}",
            'inclusions' => [
                'pagination.tpl.html' => file_get_contents($template_dir.DS.'pagination.tpl.html')
            ]
        ];
    }

	public function get_template_name() {
        return $this->template_name;
    }
}