<?php

namespace Starter\views\AdminPanel;

use common\views\TemplateView;

class AddAlbumView extends TemplateView {

    private $template_name = 'add_album.tpl.html';
    private $gallery_name;

	public function __construct($gallery) {
		parent::__construct();
		$path = $this->template->get_path();
        $this->setTemplateDir($path.DS.'templates'.DS.'admin_panel'.DS.'albums');
		$this->gallery_name = $gallery;
	}

	public function render() {
        $this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        return [
            'gallery_name' => $this->gallery_name
        ];
    }

	public function get_template_name() {
        return $this->template_name;
    }
}