<?php

namespace Starter\views\AdminPanel;

use common\views\TemplateView;

class GalleriesTableView extends TemplateView {

    private $template_name = 'galleries_table.tpl.html';

    public function __construct() {
        parent::__construct();

        $path = $this->template->get_path().DS.'templates'.DS.'admin_panel'.DS.'galleries';
        $this->setTemplateDir($path);
    }

    public function render() {
        return $this->get_template($this->template_name);
    }

    public function get_data() {
        return [];
    }

    public function get_template_name() {
        return $this->template_name;
    }
}