<?php

namespace Starter\views\site;

use common\classes\Application;
use common\classes\Response;
use common\mappers\AlbumsMapper;
use common\views\TemplateView;

class GalleryAlbumView extends TemplateView {

    private $template_name = 'album.tpl.html';
    private $album_name;

	public function __construct($name) {
		parent::__construct();
		$path = $this->template->get_path().DS.'templates'.DS.'site'.DS.'album';
        $this->setTemplateDir($path);

        $this->album_name = $name;
	}

	public function render() {
        $this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        /**
         * @var $mapper AlbumsMapper
         */
        $mapper = Application::get_class(AlbumsMapper::class);
        $album = $mapper->find_by_name($this->album_name);
        if(!$album->images) {
            Response::redirect('/');
        }
        return [
            'images' => $album->images
        ];
    }

    public function get_template_name() {
        return $this->template_name;
    }
}