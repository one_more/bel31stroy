<?php

namespace Starter\views\site;

use common\classes\Application;
use common\classes\Response;
use common\views\TemplateView;

class AtticStairsView extends TemplateView {

    private $template_name = 'stairs.tpl.html';
    private $category_name;

	public function __construct($category) {
		parent::__construct();

        $path = $this->template->get_path().DS.'templates'.DS.'site'.DS.'attic_stairs';
		$this->setTemplateDir($path);
        $this->category_name = $category;
	}

	public function render() {
        $this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        /**
         * @var $articles \Articles
         */
        $articles = Application::get_class(\Articles::class);
        $mapper = $articles->get_mapper();
        $stairs = $mapper->find_by_category_name($this->category_name);
        if(!$stairs->count()) {
            Response::redirect('/');
        }
        return [
            'stairs' => $stairs->to_array(),
            'category_name' => $this->category_name
        ];
    }

	public function get_template_name() {
        return $this->template_name;
    }
}