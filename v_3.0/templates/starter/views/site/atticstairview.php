<?php

namespace Starter\views\site;

use common\classes\Application;
use common\classes\Response;
use common\views\TemplateView;

class AtticStairView extends TemplateView {

    private $template_name = 'stair.tpl.html';
    private $stair_id;

	public function __construct($id) {
		parent::__construct();

        $path = $this->template->get_path().DS.'templates'.DS.'site'.DS.'attic_stairs';
        $this->setTemplateDir($path);

        $this->stair_id = $id;
	}

	public function render() {
        $this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        /**
         * @var $articles \Articles
         */
        $articles = Application::get_class(\Articles::class);
        $mapper = $articles->get_mapper();

        /**
         * @var $user \User
         */
        $user = Application::get_class(\User::class);
        $identity = $user->get_identity();
        $stair = $mapper->find_by_id($this->stair_id);
        if(!$stair->id) {
            Response::redirect('/');
        }
        return [
            'stair' => $stair->to_array(),
            'is_admin' => $identity->is_admin()
        ];
    }

	public function get_template_name() {
        return $this->template_name;
    }
}