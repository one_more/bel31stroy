<?php

namespace Starter\views\site;

use common\views\TemplateView;

class ContactsPageView extends TemplateView {

    private $template_name = 'contacts.tpl.html';

	public function __construct() {
		parent::__construct();
        
        $path = $this->template->get_path().DS.'templates'.DS.'site'.DS.'contacts';
		$this->setTemplateDir($path);
	}

	public function render() {
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        return [];
    }

	public function get_template_name() {
        return $this->template_name;
    }
}