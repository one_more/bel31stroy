<?php

namespace Starter\views\site;

use common\classes\Application;
use common\interfaces\View;
use common\traits\TraitView;
use common\views\TemplateView;

class MainPageView extends TemplateView {
    use TraitView;

    private $template_name = 'main.tpl.html';

	public function __construct() {
		parent::__construct();

        $template_dir = $this->template->get_path().DS.'templates'.DS.'site'.DS.'main_page';
        $this->setTemplateDir($template_dir);
	}

	public function render() {
        /**
         * @var $view View
         */
        $view = Application::get_class(FlexSliderView::class);
		$this->assign('slider', $view->render());

		$view = Application::get_class(ElastiSlideView::class);
		$this->assign('carousel', $view->render());

        $view = Application::get_class(PricesView::class);
		$this->assign('prices_list', $view->render());

        $this->assign($this->get_data());

		return $this->get_template($this->template_name);
	}

    public function get_data() {
        /**
         * @var $articles \Articles
         */
        $articles = Application::get_class(\Articles::class);
        $mapper = $articles->get_mapper();
        $article = $mapper->find_where([
            'name' => ['=', 'welcome_article']
        ])->one()->to_array();

        /**
         * @var $user \User
         */
        $user = Application::get_class(\User::class);
        $identity = $user->get_identity();
        return [
            'welcome_article' => $article,
            'is_admin' => $identity->is_admin()
        ];
    }

    public function get_template_name() {
        return $this->template_name;
    }
}