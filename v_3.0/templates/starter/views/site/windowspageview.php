<?php

namespace Starter\views\site;

use common\classes\Application;
use common\interfaces\View;
use common\views\TemplateView;

class WindowsPageView extends TemplateView {

    private $template_name = 'windows.tpl.html';

	public function __construct() {
		parent::__construct();

        $path = $this->template->get_path().DS.'templates'.DS.'site'.DS.'windows';
        $this->setTemplateDir($path);
	}

	public function render() {
        /**
         * @var $view View
         */
        $view = Application::get_class(GalleryView::class, ['windows']);
		$this->assign('gallery', $view->render());

        $images_dir = WEB_ROOT.DS.'images'.DS.'windows_facro';
        $view = Application::get_class(ListImagesView::class, [$images_dir]);
        $this->assign('images_list', $view->render());

		$this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        return [];
    }

	public function get_template_name() {
        return $this->template_name;
    }
}