<?php

namespace Starter\views\site;

use common\views\TemplateView;

class ListImagesView extends TemplateView {

    private $template_name = 'index.tpl.html';

    private $dir;

    public function __construct($dir) {
        parent::__construct();

        $this->dir = $dir;
        $path = $this->template->get_path().DS.'templates'.DS.'site'.DS.'list_images';
        $this->setTemplateDir($path);
    }

    public function render() {
        $this->assign($this->get_data());
        return $this->get_template($this->template_name);
    }

    public function get_data() {
        $images = glob($this->dir.DS.'*');
        $images = array_map(function($img){
            return str_replace(WEB_ROOT, '', $img);
        }, $images);
        return [
            'images' => $images
        ];
    }

    public function get_template_name() {
        return $this->template_name;
    }
}