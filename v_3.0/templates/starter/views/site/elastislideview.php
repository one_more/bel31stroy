<?php

namespace Starter\views\site;

use common\classes\Application;
use common\mappers\AlbumsMapper;
use common\views\TemplateView;

class ElastiSlideView extends TemplateView {

    private $template_name = 'slider.tpl.html';

    private $page;

	public function __construct($page = 'main') {
		parent::__construct();
		$template_dir = $this->template->get_path().DS.'templates'.DS.'site'.DS.'elastislide';
        $this->setTemplateDir($template_dir);
		$this->page = $page;
	}

    public function get_data() {
        switch($this->page) {
            case 'main':
            default:
                /**
                 * @var $mapper AlbumsMapper
                 */
                $mapper = Application::get_class(AlbumsMapper::class);
                $albums = $mapper->find_by_gallery_name('recent_works');
                $first_album = $albums->one();
                return [
                    'album' => $first_album->to_array()
                ];
        }
    }

    public function get_template_name() {
        return $this->template_name;
    }

	public function render() {
        $this->assign($this->get_data());
		return $this->getTemplate($this->template_name);
	}
}