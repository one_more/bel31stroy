<?php

namespace Starter\views\site;

use common\classes\Application;
use common\mappers\PricesMapper;
use common\views\TemplateView;

class PricesView extends TemplateView {

    private $template_name = 'prices.tpl.html';

	public function __construct() {
		parent::__construct();
		$template_dir = $this->template->get_path().DS.'templates'.DS.'site'.DS.'prices';
        $this->setTemplateDir($template_dir);
	}

	public function render() {
		$this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}

    public function get_data() {
        /**
         * @var $mapper PricesMapper
         */
        $mapper = Application::get_class(PricesMapper::class);
        return [
            'prices' => $mapper->get_all()->to_array()
        ];
    }

    public function get_template_name() {
        return $this->template_name;
    }
}