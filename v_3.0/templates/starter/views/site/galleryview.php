<?php

namespace Starter\views\site;

use common\classes\Application;
use common\mappers\AlbumsMapper;
use common\views\TemplateView;

class GalleryView extends TemplateView {
    
    private $template_name = 'gallery.tpl.html';
    
    private $gallery_name;
    
	public function __construct($name) {
		parent::__construct();
		$path = $this->template->get_path().DS.'templates'.DS.'site'.DS.'gallery';
        $this->setTemplateDir($path);
        
        $this->gallery_name = $name;
	}

	public function render() {
        $this->assign($this->get_data());
		return $this->get_template($this->template_name);
	}
    
    public function get_data() {
        /**
         * @var $mapper AlbumsMapper
         */
        $mapper = Application::get_class(AlbumsMapper::class);
        $albums = $mapper->find_by_gallery_name($this->gallery_name);
        return [
            'albums' => $albums->to_array(),
            'gallery_name' => $this->gallery_name
        ];
    }
    
    public function get_template_name() {
        return $this->template_name;
    }
}