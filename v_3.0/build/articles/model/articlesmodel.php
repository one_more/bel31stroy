<?php

namespace Articles\model;
use common\models\BaseModel;

/**
 * Class ArticlesModel
 * @package Articles\model
 *
 * @property int id
 * @property string name
 * @property int category
 * @property string text
 * @property string preview_img
 */
class ArticlesModel extends BaseModel {

    protected $fields = [
        'id' => null,
        'name' => null,
        'category' => null,
        'text' => null,
        'preview_img' => null
    ];
}