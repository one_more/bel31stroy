<?php

use common\classes\Application;
use Articles\mapper\ArticlesMapper;

class Articles implements \common\interfaces\Extension {
    use \common\traits\TraitExtension;

    public function __construct() {
        $this->register_autoload();
        $this->initialize();
    }

    /**
     * @return \Articles\mapper\ArticlesMapper
     * @throws InvalidArgumentException
     */
    public function get_mapper() {
        return Application::get_class(ArticlesMapper::class);
    }

    private function initialize() {
        if(defined('TESTS_ENV') || Application::is_dev()) {
            $adapter = new \common\adapters\MysqlAdapter('');
            $sql = 'SHOW TABLES';
            $tables = [CURRENT_LANG.'_articles', CURRENT_LANG.'_categories'];
            if(count(array_intersect($adapter->execute($sql)->get_arrays(), $tables)) < count($tables)) {
                $sql = file_get_contents($this->get_path().DS.'resource'.DS.'initialize.sql');
                $adapter->execute($sql);
            }
        }
    }
}