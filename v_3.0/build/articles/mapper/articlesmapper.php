<?php

namespace Articles\mapper;

use Articles\model\ArticlesModel;
use common\adapters\MysqlAdapter;
use common\classes\Error;
use common\classes\LanguageFile;
use common\collections\BaseCollection;
use common\interfaces\Extension;
use common\mappers\BaseMapper;
use Validator\LIVR;

/**
 * Class ArticlesMapper
 * @package Articles\mapper
 *
 * @property MysqlAdapter adapter
 */
class ArticlesMapper extends BaseMapper {

    /**
     * @return MysqlAdapter
     */
    public function get_adapter() {
        return new MysqlAdapter(CURRENT_LANG.'_articles');
    }

    /**
     * @param $id
     * @return ArticlesModel
     */
    public function find_by_id($id) {
        return new ArticlesModel(
            $this->adapter->select()
                ->where([
                    'id' => ['=', (int)$id],
                    'and' => [
                        'deleted' => ['=', 0]
                    ]
                ])
                ->execute()
                ->get_array()
        );
    }

    /**
     * @param array $where
     * @return BaseCollection
     */
    public function find_where(array $where) {
        $collection = new BaseCollection(ArticlesModel::class);
        $collection->load(
            $this->adapter->select()
                ->where(array_merge($where, [
                    'and' => [
                        'deleted' => ['=', 0]
                    ]
                ]))
                ->execute()
                ->get_arrays()
        );
        return $collection;
    }

    public function find_by_category_name($category) {
        $collection = new BaseCollection(ArticlesModel::class);
         $collection->load(
            $this->adapter
                ->select(['a.id', 'a.name', 'a.preview_img', 'a.category', 'a.text'])
                ->alias('a')
                ->join('left', CURRENT_LANG.'_categories as c', [
                    'a.category' => ['=', 'c.id', false]
                ])
                ->where([
                    'c.name' => ['=', $category],
                    'and' => [
                        'a.deleted' => ['=', 0],
                        'c.deleted' => ['=', 0]
                    ]
                ])
                ->execute()
                ->get_arrays()
        );
        return $collection;
    }

    /**
     * @param ArticlesModel $article
     * @return bool
     */
    public function save(ArticlesModel $article) {
        if($article->id) {
            if($this->validate($article->to_array())) {
                $this->update($article->to_array());
                return true;
            } else {
                return false;
            }
        } else {
            if($this->validate($article->to_array())) {
                $this->insert($article->to_array());
                return true;
            } else {
                return false;
            }
        }
    }

    private function validate(array $fields) {
        $validator = new LIVR([
            'name' => ['required', 'unique_name'],
            'text' => 'required'
        ]);
        $validator->registerRules([
            'unique_name' => function() use($fields) {
                return function($value) use($fields) {
                    /**
                     * @var $existed ArticlesModel
                     */
                    $existed = $this->find_where([
                        'name' => ['=', $value]
                    ])->one();
                    if($existed) {
                        if(!empty($fields['id']) && $fields['id'] === $existed->id) {
                            return null;
                        } else {
                            return 'NAME_EXISTS';
                        }
                    } else {
                        return null;
                    }
                };
            }
        ]);
        if($validator->validate($fields)) {
            return true;
        } else {
            /**
             * @var $articles Extension
             */
            $articles = \Application::get_class('Articles');
            $file = 'mapper'.DS.'articlesmapper.json';
            $lang_vars = new LanguageFile($file, $articles->get_lang_path());
            $this->validation_errors = $validator->getErrors($lang_vars['errors']);
            return false;
        }
    }

    private function update(array $fields) {
        $id = $fields['id'];
        unset($fields['id']);
        $this->adapter
            ->update($fields)
            ->where([
                'id' => ['=', (int)$id]
            ])
            ->execute();
    }

    private function insert(array $fields) {
        if(!empty($fields['id'])) {
            unset($fields['id']);
        }
        $this->adapter->insert($fields)->execute();
    }

    public function delete(ArticlesModel $model) {
        $this->adapter
            ->delete()
            ->where([
                'id' => ['=', $model->id]
            ])
            ->execute();
    }
}