<?php
include_once(realpath(dirname($_SERVER['DOCUMENT_ROOT'].'../').'/resource/defines.php'));

// class to browse images in a folder (for CKEditor plugin)
// from: http://coursesweb.net/javascript/
class ImgBrowse {
  	private $img_types = ['bmp', 'gif', 'jpg', 'jpe', 'jpeg', 'png'];    // allowed image extensions
  	private $img_dir = '';     // current folder (in $root) with images
	private $root;

  function __construct() {
	  $this->root = ROOT_PATH.DS.'www';
	  if($_REQUEST['imgdr']) {
		  $this->img_dir = $_REQUEST['imgdr'];
	  } else {
		  $this->img_dir = 'starter'.DS.'images';
	  }
  }

  // return two-dimensional array with folders-list and images in specified $img_dir
  public function getMenuImgs() {
    $result = ['menu'=>'', 'imgs'=>''];
    try{
      $iterator = new DirectoryIterator($this->root.DS.$this->img_dir);
    }
    catch(Exception $e) {
      return 'error: wrong image dir';
    }

    foreach($iterator as $fileobj) {
      $name = $fileobj->getFilename();

      // if image file, else, directory (but not . or ..), add data in $re
      if($fileobj->isFile() && in_array($fileobj->getExtension(), $this->img_types)) {
		  $src = DS.$this->img_dir.DS.$name;
		  $result['imgs'] .= "<span><img src=\"{$src}\" alt=\"{$name}\" height=\"50\" />{$name}</span>";
	  } else if($fileobj->isDir() && !$fileobj->isDot()) {
		  $title = $this->img_dir.DS.$name;
		  $result['menu'] .= '<li><span title="'.$title.'">'.$name.'</span></li>';
	  }
    }
    if($result['menu'] != '') $result['menu'] = '<ul>'. $result['menu'] .'</ul>';
    if($result['imgs'] == '') $result['imgs'] = '<h1>No Images</h1>';
    return $result;
  }
}

// uses the imgbrowse class
$obj = new imgbrowse;
$content = $obj->getMenuImgs();
echo json_encode($content);